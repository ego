#include "egoplayer.h"

/*
 * manages the use of the function NPN_PluginThreadAsyncCall
 * and its alternative implementation with XEvents
 *
 * 1: enable plugin for
 * - gecko < 1.9
 * - webkit
 *
 * 0:	plugin runs only with
 * - gecko >= 1.9
 */
#define COMPLIANT 0

/*
 * manages the use of the windowed and windowless mode of plugins
 *
 * 1: plugin runs in windowed mode
 * - gecko < 1.9
 *
 * 0: plugin runs in windowless mode (transparency)
 * - gecko >= 1.9
 */
#define WINDOWED 1

void
mep_asynccall (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	ecore_main_loop_iterate ();
	if (!pi->done)
	{
		usleep (1000); // max 100 fps
#if COMPLIANT
		XSendEvent (
			pi->display,
			pi->window,
			1,
			NoEventMask,
			(XEvent *) &(pi->msg));
#else
		NPN_PluginThreadAsyncCall (pi->instance, mep_asynccall, (void *) pi);
#endif /* COMPLIANT */
	}
}

int
mep_animator (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	evas_render (pi->evas);
	return 1;
}

#if WINDOWED
void
mep_event_handler (Widget xtwidget, void *data, XEvent *xevent, Boolean *b)
{
	PluginInstance *pi = (PluginInstance *) data;
	NPP_HandleEvent (pi->instance, xevent);
}
#endif /* WINDOWED */

char*
NPP_GetMIMEDescription (void)
{
	return (MIME_TYPES_HANDLED);
}

NPError
NPP_GetValue (NPP instance, NPPVariable variable, void *value)
{
	PluginInstance *pi;
	NPError err = NPERR_NO_ERROR;

	switch (variable) {
		case NPPVpluginNameString:
			*((char **) value) = PLUGIN_NAME;
			break;
		case NPPVpluginDescriptionString:
			*((char **) value) = PLUGIN_DESCRIPTION;
			break;
		default:
			err = NPERR_GENERIC_ERROR;
	}
	return err;
}

int16
NPP_HandleEvent (NPP instance, void *xevent)
{
	PluginInstance *pi = (PluginInstance *) instance->pdata;
	Evas *evas = pi->evas;
	XEvent ev = *((XEvent *) xevent);

	switch (ev.type) {
		case KeyPress:
 			printf ("KeyPress\n");
			break;					
		case KeyRelease:
 			printf ("KeyRelease\n");
			break;
		case ButtonPress:
 			printf ("ButtonPress\n");
			evas_event_feed_mouse_move(evas, ev.xbutton.x, ev.xbutton.y, 0, NULL);
		  evas_event_feed_mouse_down(evas, ev.xbutton.button, EVAS_BUTTON_NONE, 0, NULL);
			break;		
		case ButtonRelease:
 			printf ("ButtonRelease\n");
		  evas_event_feed_mouse_move(evas, ev.xbutton.x, ev.xbutton.y, 0, NULL);
		  evas_event_feed_mouse_up(evas, ev.xbutton.button, EVAS_BUTTON_NONE, 0, NULL);
			break;
		case MotionNotify:
 			//printf ("MotionNotify\n");
		  evas_event_feed_mouse_move(evas, ev.xmotion.x, ev.xmotion.y, 0, NULL);
			break;	
		case EnterNotify:
 			printf ("EnterNotify\n");
			break;	
		case LeaveNotify:
 			printf ("LeaveNotify\n");
			break;
		case FocusIn:
 			printf ("FocusIn\n");
			break;
		case FocusOut:
 			printf ("FocusOut\n");
			break;
		case KeymapNotify:
 			printf ("KeymapNotify\n");
			break;
		case Expose:
 			printf ("Expose\n");
		  evas_damage_rectangle_add(evas,
					    ev.xexpose.x,
					    ev.xexpose.y,
					    ev.xexpose.width,
					    ev.xexpose.height);
			break;
		case GraphicsExpose:
 			printf ("GraphicsExpose\n");
			break;
		case NoExpose:
 			printf ("NoExpose\n");
			break;
		case VisibilityNotify:
 			printf ("VisibilityNotify\n");
			break;
		case CreateNotify:
 			printf ("CreateNotify\n");
			break;
		case DestroyNotify:
 			printf ("DestroyNotify\n");
			break;
		case UnmapNotify:
 			printf ("UnmapNotify\n");
			break;
		case MapNotify:
 			printf ("MapNotify\n");
			break;
		case MapRequest:
 			printf ("MapRequest\n");
			break;
		case ReparentNotify:
 			printf ("ReparentNotify\n");
			break;
		case ConfigureNotify:
 			printf ("ConfigureNotify\n");
		  evas_output_size_set(evas,
				       ev.xconfigure.width,
				       ev.xconfigure.height);
			break;
		case ConfigureRequest:
 			printf ("ConfigureRequest\n");
			break;
		case GravityNotify:
 			printf ("GravityNotify\n");
			break;
		case ResizeRequest:
 			printf ("ResizeRequest\n");
			break;
		case CirculateNotify:
 			printf ("CirculateNotify\n");
			break;
		case CirculateRequest:
 			printf ("CirculateRequest\n");
			break;
		case PropertyNotify:
 			printf ("PropertyNotify\n");
			break;
		case SelectionClear:
 			printf ("SelectionClear\n");
			break;
		case SelectionRequest:
 			printf ("SelectionRequest\n");
			break;
		case SelectionNotify:
 			printf ("SelectionNotify\n");
			break;
		case ColormapNotify:
 			printf ("ColormapNotify\n");
			break;
		case ClientMessage:
#if COMPLIANT
 			//printf ("ClientMessage\n");
			if (ev.xclient.message_type == pi->atom)
				mep_asynccall (pi);
#endif
			break;
		case MappingNotify:
 			printf ("MappingNotify\n");
			break;
		case LASTEvent:
 			printf ("LASTEvent\n");
			break;							
	}

	return TRUE;
}

NPError
NPP_Initialize (void)
{
	const char *ref = "ego";
	eet_init ();
	ecore_init ();
	ecore_config_init (ref);
	evas_init ();
	edje_init ();
	elm_init (1, &ref);

  return NPERR_NO_ERROR;
}

#ifdef OJI
jref
NPP_GetJavaClass ()
{
	return NULL;
}
#endif

void
NPP_Shutdown (void)
{
	elm_shutdown ();
	edje_shutdown ();
	evas_shutdown ();
	ecore_config_shutdown ();
	ecore_shutdown ();
	eet_shutdown ();
}

NPError 
NPP_New (NPMIMEType pluginType,
    NPP instance,
    uint16 mode,
    int16 argc,
    char* argn[],
    char* argv[],
    NPSavedData* saved)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;
	instance->pdata = NPN_MemAlloc (sizeof (PluginInstance));
	
	PluginInstance *pi = (PluginInstance*) instance->pdata;
	if (pi == NULL) 
		return NPERR_OUT_OF_MEMORY_ERROR;
	memset(pi, 0, sizeof (PluginInstance));

	/* mode is NP_EMBED, NP_FULL, or NP_BACKGROUND (see npapi.h) */
	pi->mode = mode;
	pi->type = strdup ("application/x-ego");
	pi->instance = instance;

	pi->pluginsPageUrl = NULL;
	pi->exists = FALSE;

	/* Parse argument list passed to plugin instance */
	/* We are interested in these arguments
	 *  PLUGINSPAGE = <url>
	 */
	while (argc > 0)
	{
		argc --;
		if (argv[argc] != NULL)
		{
			if (!PL_strcasecmp (argn[argc], "PLUGINSPAGE"))
				pi->pluginsPageUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "PLUGINURL"))
				pi->pluginsFileUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "CODEBASE"))
				pi->pluginsPageUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "CLASSID"))
				pi->pluginsFileUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "HIDDEN"))
				pi->pluginsHidden = (!PL_strcasecmp (argv[argc], "TRUE"));
			else if (!PL_strcasecmp (argn[argc], "DATA"))
				pi->data = strdup (argv[argc]);
		}
	}

	// create evas for this plugin instance
	pi->evas = evas_new ();

	pi->L = luaL_newstate ();
	ego_sandbox (pi->L, 4);
	luaopen_ego (pi->L);

	lua_pushstring (pi->L, pi->data);
	lua_setfield (pi->L, LUA_REGISTRYINDEX, "ego_file");

	return NPERR_NO_ERROR;
}

NPError 
NPP_Destroy (NPP instance, NPSavedData** save)
{
	if (instance == NULL)
			return NPERR_INVALID_INSTANCE_ERROR;

	PluginInstance *pi = (PluginInstance*) instance->pdata;

	pi->done = 1;

	if (pi != NULL) {
		if (pi->type)
			NPN_MemFree(pi->type);
		if (pi->pluginsPageUrl)
			NPN_MemFree(pi->pluginsPageUrl);
		if (pi->pluginsFileUrl)
			NPN_MemFree(pi->pluginsFileUrl);
		if (pi->L)
			lua_close (pi->L);
		if (pi->ego_obj)
			evas_object_del (pi->ego_obj);
		if (pi->theme_obj)
			evas_object_del (pi->theme_obj);
		if (pi->anim)
			ecore_animator_del (pi->anim);
		if (pi->evas)
			evas_free (pi->evas);
		NPN_MemFree(instance->pdata);
		instance->pdata = NULL;
	}

	return NPERR_NO_ERROR;
}

static void
_config_fps_read (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	int fps = ecore_config_int_get ("fps");
	ecore_animator_frametime_set (1.0 / (double) fps);
	edje_frametime_set (1.0 / (double) fps);
}

static void
_config_theme_read (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	char path[256];
	sprintf (path, "%s/%s", THEMEDIR, ecore_config_theme_get ("theme"));
	edje_object_file_set (pi->theme_obj, path, "ego/theme");	
}

static void
_config_engine_read (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	int method = evas_render_method_lookup (ecore_config_string_get ("engine"));
	if (method != evas_output_method_get (pi->evas))
	{
		if (method == evas_render_method_lookup ("software_x11"))
		{
			evas_output_method_set (pi->evas, method);
			evas_output_size_set (pi->evas, pi->width, pi->height);
			evas_output_viewport_set (pi->evas, 0, 0, pi->width, pi->height);

			printf ("software_x11\n");
			Evas_Engine_Info_Software_X11 *einfo = (Evas_Engine_Info_Software_X11 *) evas_engine_info_get (pi->evas);
			/* the following is specific to the engine */
			einfo->info.connection = pi->display;
			einfo->info.visual = pi->visual;
			einfo->info.colormap = pi->colormap;
			einfo->info.drawable = (Drawable) pi->window;
			einfo->info.depth = pi->depth;
			einfo->info.rotation = 0;
			einfo->info.debug = 0;
			evas_engine_info_set(pi->evas, (Evas_Engine_Info *) einfo);
		}
		else if (method == evas_render_method_lookup ("gl_x11"))
		{
			evas_output_method_set (pi->evas, method);
			evas_output_size_set (pi->evas, pi->width, pi->height);
			evas_output_viewport_set (pi->evas, 0, 0, pi->width, pi->height);

			printf ("gl_x11\n");
			Evas_Engine_Info_GL_X11 *einfo = (Evas_Engine_Info_GL_X11 *) evas_engine_info_get (pi->evas);
			/* the following is specific to the engine */
			einfo->info.display = pi->display;
			einfo->info.visual = pi->visual;
			einfo->info.colormap = pi->colormap;
			einfo->info.drawable = (Drawable) pi->window;
			einfo->info.depth = pi->depth;
			evas_engine_info_set(pi->evas, (Evas_Engine_Info *) einfo);
		}
	}
}

NPError 
NPP_SetWindow (NPP instance, NPWindow *window)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	PluginInstance *pi = (PluginInstance *) instance->pdata;
	if (pi == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	NPSetWindowCallbackStruct *ws_info = (NPSetWindowCallbackStruct *) window->ws_info;

	if (pi->window == (Window) window->window) {
		/* The page with the plugin is being resized.
			 Save any UI information because the next time
			 around expect a SetWindow with a new window
			 id.
		*/
		/*
		fprintf(stderr, "plugin received window resize.\n");
		fprintf(stderr, "Window=(%i)\n", (int)window);
		fprintf(stderr, "W=(%i) H=(%i)\n",
				(int)window->width, (int)window->height);
		*/

		return NPERR_NO_ERROR;
	} else {
		pi->window = (Window) window->window;
		pi->x = window->x;
		pi->y = window->y;
		pi->width = window->width;
		pi->height = window->height;
		pi->display = ws_info->display;
		pi->visual = ws_info->visual;
		pi->depth = ws_info->depth;
		pi->colormap = ws_info->colormap;

		char path[256];
		sprintf (path, "%s/%s", CONFIGDIR, "config.edb");
		ecore_config_file_load (path);

		_config_fps_read (pi);
		_config_engine_read (pi);

		// add animator to render evas
		pi->anim = ecore_animator_add (mep_animator, pi);
		ecore_animator_frametime_set (1.0 / 25.0);
		evas_event_feed_mouse_in (pi->evas, 0, NULL);

		NPN_GetURL (instance, pi->data, NULL);
		
		// theme
		pi->theme_obj = edje_object_add (pi->evas);
		//edje_object_file_set (pi->theme_obj, THEMEDIR"/default.edj", "ego/theme");
		_config_theme_read (pi);
		edje_object_part_text_set (pi->theme_obj, "message", "loading");
		edje_object_signal_emit (pi->theme_obj, "idle,start", "");
		evas_object_resize (pi->theme_obj, pi->width, pi->height);
		evas_object_focus_set (pi->theme_obj, 1);
		evas_object_show (pi->theme_obj);
		
		// set up message system
#if WINDOWED
		Widget xtwidget = XtWindowToWidget (pi->display, pi->window);
		long event_mask = NoEventMask |
											ExposureMask |
											ButtonPressMask |
											ButtonReleaseMask |
											PointerMotionMask |
											StructureNotifyMask |
											SubstructureNotifyMask |
											EnterWindowMask |
											LeaveWindowMask;

		XSelectInput (pi->display, pi->window, event_mask);
		XtAddEventHandler (xtwidget, event_mask, True, mep_event_handler, pi);
#endif /* WINDOWED */

#if COMPLIANT
		pi->atom = XInternAtom (pi->display, "edje_atom", False);
		pi->msg.type = ClientMessage;
		pi->msg.display = pi->display;
		pi->msg.window = pi->window;
		pi->msg.message_type = pi->atom;
		pi->msg.format = 32;
		pi->msg.data.l[0] = 0;
#endif /* COMPLIANT */

		pi->done = 0;

		// initialize asynccallback loop
		mep_asynccall (pi);
	}

	return NPERR_NO_ERROR;
}

NPError 
NPP_NewStream (NPP instance,
          NPMIMEType type,
          NPStream *stream, 
          NPBool seekable,
          uint16 *stype)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	if (!strcmp (type, "application/x-ego"))
	{
		*stype = NP_ASFILEONLY; //ONLY
	}

	printf ("NPP_NewStream: %s %d\n", type, *stype);

	return NPERR_NO_ERROR;
}

int32 
NPP_WriteReady (NPP instance, NPStream *stream)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	printf ("NPP_WriteReady\n");

	/* We don't want any data, kill the stream */
	NPN_DestroyStream(instance, stream, NPRES_DONE);

	/* Number of bytes ready to accept in NPP_Write() */
	return -1L;   /* don't accept any bytes in NPP_Write() */
}

int32 
NPP_Write (NPP instance, NPStream *stream, int32 offset, int32 len, void *buffer)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	printf ("NPP_Write\n");

	/* We don't want any data, kill the stream */
	NPN_DestroyStream(instance, stream, NPRES_DONE);

	return -1L;   /* don't accept any bytes in NPP_Write() */
}

NPError 
NPP_DestroyStream (NPP instance, NPStream *stream, NPError reason)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	/***** Insert NPP_DestroyStream code here *****\
	PluginInstance* pi;
	pi = (PluginInstance*) instance->pdata;
	\**********************************************/

	return NPERR_NO_ERROR;
}

void 
NPP_StreamAsFile (NPP instance, NPStream *stream, const char* fname)
{
	/***** Insert NPP_StreamAsFile code here *****\
	PluginInstance* pi;
	if (instance != NULL)
			pi = (PluginInstance*) instance->pdata;
	\*********************************************/
	printf ("NPP_StreamAsFile: %s\n", fname);
	PluginInstance *pi = (PluginInstance *) instance->pdata;

	if (pi->data)
		NPN_MemFree (pi->data);
	pi->data = fname;
	edje_object_part_text_set (pi->theme_obj, "message", "loaded");
	edje_object_signal_emit (pi->theme_obj, "idle,stop", "");

	// load ego file
	Eet_File *fl_file = eet_open (pi->data, EET_FILE_MODE_READ);
	void *globbuf;
	int globbufsize;

	globbuf = eet_read (fl_file, "ego/bytecode", &globbufsize);
	printf ("globbufsize: %d\n", globbufsize);
	
	int num, i;
	char **keys = eet_list (fl_file, "*", &num);
	for (i = 0; i < num; i++)
		printf ("%d: %s\n", i, keys[i]);
	free (keys);

	// ego
	if (luaL_loadbuffer (pi->L, globbuf, globbufsize, NULL))
		printf ("ego load chunk error: %s\n", luaL_checkstring (pi->L, -1));
	pi->ego_obj = ego_object_add (pi->evas, pi->L);
	evas_object_show (pi->ego_obj);
	edje_object_part_swallow (pi->theme_obj, "content", pi->ego_obj);

	free (globbuf);
	eet_close (fl_file);
}

void
NPP_URLNotify (NPP instance, const char* url,
                NPReason reason, void* notifyData)
{
	/***** Insert NPP_URLNotify code here *****\
	PluginInstance* pi;
	if (instance != NULL)
			pi = (PluginInstance*) instance->pdata;
	\*********************************************/
	printf ("URLNotify: %s\n", url);
}

void 
NPP_Print (NPP instance, NPPrint* printInfo)
{
	if(printInfo == NULL)
		return;

	if (instance != NULL) {
	/***** Insert NPP_Print code here *****\
			PluginInstance* pi = (PluginInstance*) instance->pdata;
	\**************************************/
	
		if (printInfo->mode == NP_FULL) {
				/*
				 * PLUGIN DEVELOPERS:
				 *  If your plugin would like to take over
				 *  printing completely when it is in full-screen mode,
				 *  set printInfo->pluginPrinted to TRUE and print your
				 *  plugin as you see fit.  If your plugin wants Netscape
				 *  to handle printing in this case, set
				 *  printInfo->pluginPrinted to FALSE (the default) and
				 *  do nothing.  If you do want to handle printing
				 *  yourself, printOne is true if the print button
				 *  (as opposed to the print menu) was clicked.
				 *  On the Macintosh, platformPrint is a THPrint; on
				 *  Windows, platformPrint is a structure
				 *  (defined in npapi.h) containing the printer name, port,
				 *  etc.
				 */

/***** Insert NPP_Print code here *****\
				void* platformPrint =
						printInfo->print.fullPrint.platformPrint;
				NPBool printOne =
						printInfo->print.fullPrint.printOne;
\**************************************/
				
				/* Do the default*/
				printInfo->print.fullPrint.pluginPrinted = FALSE;
		}
		else {  /* If not fullscreen, we must be embedded */
				/*
				 * PLUGIN DEVELOPERS:
				 *  If your plugin is embedded, or is full-screen
				 *  but you returned false in pluginPrinted above, NPP_Print
				 *  will be called with mode == NP_EMBED.  The NPWindow
				 *  in the printInfo gives the location and dimensions of
				 *  the embedded plugin on the printed page.  On the
				 *  Macintosh, platformPrint is the printer port; on
				 *  Windows, platformPrint is the handle to the printing
				 *  device context.
				 */

/***** Insert NPP_Print code here *****\
				NPWindow* printWindow =
						&(printInfo->print.embedPrint.window);
				void* platformPrint =
						printInfo->print.embedPrint.platformPrint;
\**************************************/
		}
	}
}

