#define TARGET              "_blank"
#define MIME_TYPES_HANDLED  "application/x-ego:E Goes Online"
#define PLUGIN_NAME         "ego plugin"
#define PLUGIN_DESCRIPTION  "This Plugin is able to preview ego files (E Goes Online)."

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <npapi.h>
#include <npupp.h>
#include <npruntime.h>

#include <X11/Intrinsic.h>
#include <X11/Xlib.h>

#include <Eet.h>
#include <Ecore.h>
#include <Ecore_Config.h>
#include <Evas.h>
#include <Evas_Engine_Software_X11.h>
#include <Evas_Engine_GL_X11.h>
#include <Edje.h>
#include <Elementary.h>
#include "ego.h"

typedef struct _PluginInstance
{
    uint16 mode;
    Window window;
    Display *display;
    uint32 x, y;
    uint32 width, height;
    NPMIMEType type;
    char *message;

    NPP instance;
    char *pluginsPageUrl;
    char *pluginsFileUrl;
    NPBool pluginsHidden;
    Visual* visual;
    Colormap colormap;
    unsigned int depth;

		Atom atom;
		Ecore_Animator *anim;
		Evas *evas;
		lua_State *L;
		char *data;
		XClientMessageEvent msg;
		pthread_t trigger_id;
		int done;

		Evas_Object *ego_obj;
		Evas_Object *theme_obj;
		int engine_toggle;

    NPBool exists;  /* Does the widget already exist? */
    int action;     /* What action should we take? (GET or REFRESH) */

} PluginInstance;

void mep_asynccall (void *userData);
int mep_animator (void *data);

