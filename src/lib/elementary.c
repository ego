#include "class.h"
#include "object.h"
#include "macro.h"
#include <Evas.h>

/*
 * Elementary callback
 */

static void
elm_callback (void *data, Evas_Object *obj, void *event_info)
{
	luaobj_Ref *ref1 = evas_object_data_get (obj, "luaobj");
	luaobj_Ref *ref2 = data;
	luaobj_get_ref (ref1->L, ref2);
	luaobj_get_ref (ref1->L, ref1);
	if (lua_pcall (ref1->L, 1, 0, 0))
		fprintf (stderr, "elm_callback: %s\n", lua_tostring (ref1->L, -1));
}
#define ELM_CB(FUNC, NAME) \
static int \
FUNC (lua_State *L) \
{ \
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1); \
	evas_object_smart_callback_add ( \
		obj->data, \
		NAME, \
		elm_callback, \
		(void *) luaobj_new_ref (L, 2)); \
	return 0; \
}

/*
 * Elementary Icon
 */

static int
licon_set_file (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int key = luaL_checknumber (L, 2);
	char buf [256];
	sprintf (buf, "images/%i", key);
	lua_getfield (L, LUA_REGISTRYINDEX, "ego_file");
	const char *file = luaL_checkstring (L, -1);
  elm_icon_file_set (obj->data, file, buf);
  return 0;
}

SET_STRING (licon_set_standard, elm_icon_standard_set);
SET_BOOL (licon_set_smooth, elm_icon_smooth_set);
SET_BOOL (licon_set_no_scale, elm_icon_no_scale_set);
SET_TAB2BOOL (licon_set_scale, elm_icon_scale_set);
SET_BOOL (licon_set_fill_outside, elm_icon_fill_outside_set);

const struct luaL_Reg lNil[] = {
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lIcon_set[] = {
	{"file", licon_set_file},
	{"standard", licon_set_standard},
	{"smooth", licon_set_smooth},
	{"no_scale", licon_set_no_scale},
	{"scale", licon_set_scale},
	{"fill_outside", licon_set_fill_outside},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mIcon = {
  lNil,
  lNil,
  lIcon_set,
  lNil
};

const luaobj_Reg *cIcon[] = {
  &mClass,
  &mObject,
  &mIcon,
  NULL
};

/*
 * Elementary Box
 */

SET_BOOL (lbox_set_horizontal, elm_box_horizontal_set);
SET_BOOL (lbox_set_homogenous, elm_box_homogenous_set);
FN_OBJ (lbox_fn_pack_start, elm_box_pack_start);
FN_OBJ (lbox_fn_pack_end, elm_box_pack_end);
FN_2OBJ (lbox_fn_pack_before, elm_box_pack_before);
FN_2OBJ (lbox_fn_pack_after, elm_box_pack_after);

const struct luaL_Reg lBox2_set[] = {
	{"horizontal", lbox_set_horizontal},
	{"homogenous", lbox_set_homogenous},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lBox2_fn[] = {
	{"pack_start", lbox_fn_pack_start},
	{"pack_end", lbox_fn_pack_end},
	{"pack_before", lbox_fn_pack_before},
	{"pack_after", lbox_fn_pack_after},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mBox2 = {
  lNil,
  lNil,
  lBox2_set,
  lBox2_fn
};

const luaobj_Reg *cBox2[] = {
  &mClass,
  &mObject,
  &mBox2,
  NULL
};

/*
 * Elementary Button
 */

SET_STRING (lbutton_set_label, elm_button_label_set);
SET_OBJ (lbutton_set_icon, elm_button_icon_set);
ELM_CB (lbutton_set_clicked, "clicked")

const struct luaL_Reg lButton_set[] = {
	{"label", lbutton_set_label},
	{"icon", lbutton_set_icon},
	{"clicked", lbutton_set_clicked},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mButton = {
  lNil,
  lNil,
  lButton_set,
  lNil
};

const luaobj_Reg *cButton[] = {
  &mClass,
  &mObject,
  &mButton,
  NULL
};

/*
 * Elementary Scroller
 */

SET_OBJ (lscroller_set_content, elm_scroller_content_set);
SET_TAB2BOOL (lscroller_set_content_min_limit, elm_scroller_content_min_limit);

const struct luaL_Reg lScroller_set[] = {
	{"content", lscroller_set_content},
	{"content_min_limit", lscroller_set_content_min_limit},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mScroller = {
  lNil,
  lNil,
  lScroller_set,
  lNil
};

const luaobj_Reg *cScroller[] = {
  &mClass,
  &mObject,
  &mScroller,
  NULL
};

/*
 * Elementary Label
 */

SET_STRING (llabel_set_label, elm_label_label_set);

const struct luaL_Reg lLabel_set[] = {
	{"label", llabel_set_label},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mLabel = {
  lNil,
  lNil,
  lLabel_set,
  lNil
};

const luaobj_Reg *cLabel[] = {
  &mClass,
  &mObject,
  &mLabel,
  NULL
};

/*
 * Elementary Toggle
 */

SET_STRING (ltoggle_set_label, elm_toggle_label_set);
SET_OBJ (ltoggle_set_icon, elm_toggle_icon_set);
SET_TAB2STRING (ltoggle_set_states_labels, elm_toggle_states_labels_set);
SET_BOOL (ltoggle_set_state, elm_toggle_state_set);
ELM_CB (ltoggle_set_changed, "changed")

static int
ltoggle_get_state (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	int state;
	elm_toggle_state_pointer_set (obj->data, &state);
	lua_pushnumber (L, state);
	return 1;
}

const struct luaL_Reg lToggle_get[] = {
	{"state", ltoggle_get_state},
	{NULL, NULL}	// sentinel
};

const struct luaL_Reg lToggle_set[] = {
	{"label", ltoggle_set_label},
	{"icon", ltoggle_set_icon},
	{"states_labels", ltoggle_set_states_labels},
	{"state", ltoggle_set_state},
	{"changed", ltoggle_set_changed},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mToggle = {
  lNil,
  lToggle_get,
  lToggle_set,
  lNil
};

const luaobj_Reg *cToggle[] = {
  &mClass,
  &mObject,
  &mToggle,
  NULL
};

/*
 * Elementary Frame
 */

SET_STRING (lframe_set_label, elm_frame_label_set);
SET_OBJ (lframe_set_content, elm_frame_content_set);

const struct luaL_Reg lFrame_set[] = {
	{"label", lframe_set_label},
	{"content", lframe_set_content},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mFrame = {
  lNil,
  lNil,
  lFrame_set,
  lNil
};

const luaobj_Reg *cFrame[] = {
  &mClass,
  &mObject,
  &mFrame,
  NULL
};

/*
 * Elementary Table
 */

SET_BOOL (ltable_set_homogenous, elm_table_homogenous_set);

static int
ltable_fn_pack (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Object *pac = (luaobj_Object *) lua_touserdata (L, 2);
	elm_table_pack (
		obj->data, pac->data,
		luaL_checkint (L, 3), luaL_checkint (L, 4), // x, y
		luaL_checkint (L, 5), luaL_checkint (L, 6)); // w, h
	return 0;
}

const struct luaL_Reg lTable2_set[] = {
	{"homogenous", ltable_set_homogenous},
	{NULL, NULL}	// sentinel
};

const struct luaL_Reg lTable2_fn[] = {
	{"pack", ltable_fn_pack},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mTable2 = {
  lNil,
  lNil,
  lTable2_set,
  lTable2_fn
};

const luaobj_Reg *cTable2[] = {
  &mClass,
  &mObject,
  &mTable2,
  NULL
};

GET_STRING (lentry_get_entry, elm_entry_entry_get);
GET_STRING (lentry_get_selection, elm_entry_selection_get);
SET_BOOL (lentry_set_single_line, elm_entry_single_line_set);
SET_BOOL (lentry_set_password, elm_entry_password_set);
SET_STRING (lentry_set_entry, elm_entry_entry_set);
SET_BOOL (lentry_set_line_wrap, elm_entry_line_wrap_set);
SET_BOOL (lentry_set_editable, elm_entry_editable_set);
FN_STRING (lentry_fn_entry_insert, elm_entry_entry_insert);

ELM_CB (lentry_set_changed, "changed")
ELM_CB (lentry_set_selection_start, "selection,start")
ELM_CB (lentry_set_selection_changed, "selection,changed")
ELM_CB (lentry_set_selection_cleared, "selection,cleared")
ELM_CB (lentry_set_selection_paste, "selection,paste")
ELM_CB (lentry_set_selection_copy, "selection,copy")
ELM_CB (lentry_set_selection_cut, "selection,cut")
ELM_CB (lentry_set_cursor_changed, "cursor,changed")
ELM_CB (lentry_set_anchor_clicked, "anchor,clicked")
ELM_CB (lentry_set_activated, "activated")

const struct luaL_Reg lEntry_get[] = {
	{"entry", lentry_get_entry},
	{"selection", lentry_get_selection},
	{NULL, NULL}	// sentinel
};

const struct luaL_Reg lEntry_set[] = {
	{"single_line", lentry_set_single_line},
	{"password", lentry_set_password},
	{"entry", lentry_set_entry},
	{"line_wrap", lentry_set_line_wrap},
	{"editable", lentry_set_editable},

	{"changed", lentry_set_changed},
	{"selection,start", lentry_set_selection_start},
	{"selection,changed", lentry_set_selection_changed},
	{"selection,cleard", lentry_set_selection_cleared},
	{"selection,paste", lentry_set_selection_paste},
	{"selection,copy", lentry_set_selection_copy},
	{"selection,cut", lentry_set_selection_cut},
	{"cursor,changed", lentry_set_cursor_changed},
	{"anchor,clicked", lentry_set_anchor_clicked},
	{"activated", lentry_set_activated},

	{NULL, NULL}	// sentinel
};

const struct luaL_Reg lEntry_fn[] = {
	{"entry_insert", lentry_fn_entry_insert},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mEntry = {
  lNil,
  lEntry_get,
  lEntry_set,
  lEntry_fn
};

const luaobj_Reg *cEntry[] = {
  &mClass,
  &mObject,
  &mEntry,
  NULL
};

/*
 * Elementary Bubble
 */

SET_STRING (lbubble_set_label, elm_bubble_label_set);
SET_STRING (lbubble_set_info, elm_bubble_info_set);
SET_OBJ (lbubble_set_content, elm_bubble_content_set);
SET_OBJ (lbubble_set_icon, elm_bubble_icon_set);

const struct luaL_Reg lBubble_set[] = {
	{"label", lbubble_set_label},
	{"content", lbubble_set_content},
	{"info", lbubble_set_info},
	{"icon", lbubble_set_icon},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mBubble = {
  lNil,
  lNil,
  lBubble_set,
  lNil
};

const luaobj_Reg *cBubble[] = {
  &mClass,
  &mObject,
  &mBubble,
  NULL
};

/*
 * Elementary Hover
 */

SET_OBJ (lhover_set_target, elm_hover_target_set);
SET_OBJ (lhover_set_parent, elm_hover_parent_set);
SET_STRING (lhover_set_style, elm_hover_style_set);
ELM_CB (lhover_set_clicked, "clicked")

static int
lhover_set_content (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
  luaobj_Object *con = (luaobj_Object *) lua_touserdata (L, -1);
	elm_hover_content_set (
		obj->data,
		luaL_checkstring (L, -2),
		con->data);
	return 0;
}

const struct luaL_Reg lHover_set[] = {
	{"target", lhover_set_target},
	{"parent", lhover_set_parent},
	{"content", lhover_set_content},
	{"style", lhover_set_style},
	{"clicked", lhover_set_clicked},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mHover = {
  lNil,
  lNil,
  lHover_set,
  lNil
};

const luaobj_Reg *cHover[] = {
  &mClass,
  &mObject,
  &mHover,
  NULL
};

/*
 * Elementary Layout
 */

static int
llayout_set_group (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  const char *group = luaL_checkstring (L, 2);
	lua_getfield (L, LUA_REGISTRYINDEX, "ego_file");
	const char *file = luaL_checkstring (L, -1);
  elm_layout_file_set (obj->data, file, group);
	return 0;
}

static int
llayout_set_content (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
  luaobj_Object *con = (luaobj_Object *) lua_touserdata (L, -1);
	elm_layout_content_set (
		obj->data,
		luaL_checkstring (L, -2),
		con->data);
	return 0;
}

const struct luaL_Reg lLayout_set[] = {
	{"group", llayout_set_group},
	{"content", llayout_set_content},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mLayout = {
  lNil,
  lNil,
  lLayout_set,
  lNil
};

const luaobj_Reg *cLayout[] = {
  &mClass,
  &mObject,
  &mLayout,
  NULL
};

/*
 * Elementary Clock
 */

SET_TAB3INTEGER (lclock_set_time, elm_clock_time_set);
GET_TAB3INTEGER (lclock_get_time, elm_clock_time_get);
SET_BOOL (lclock_set_edit, elm_clock_edit_set);
SET_BOOL (lclock_set_show_am_pm, elm_clock_show_am_pm_set);
SET_BOOL (lclock_set_show_seconds, elm_clock_show_seconds_set);

const struct luaL_Reg lClock_set[] = {
	{"time", lclock_set_time},
	{"edit", lclock_set_edit},
	{"show_am_pm", lclock_set_show_am_pm},
	{"show_seconds", lclock_set_show_seconds},
	{NULL, NULL}	// sentinel
};

const struct luaL_Reg lClock_get[] = {
	{"time", lclock_get_time},
	{NULL, NULL}	// sentinel
};

const luaobj_Reg mClock = {
  lNil,
  lClock_get,
  lClock_set,
  lNil
};

const luaobj_Reg *cClock[] = {
  &mClass,
  &mObject,
  &mClock,
  NULL
};

