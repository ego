#include "class.h"
#include "object.h"
#include <Evas.h>
#include "macro.h"

/*
 * object callback methods
 */

inline void
lobject_cb_macro (lua_State * L, Evas_Object * obj, const char *key)
{
  luaobj_Ref *ref_fun = (luaobj_Ref *) evas_object_data_get (obj, key);
  luaobj_Ref *ref_obj = (luaobj_Ref *) evas_object_data_get (obj, "luaobj");
  luaobj_get_ref (L, ref_fun);
  luaobj_get_ref (L, ref_obj);
}

void
lobject_cb_mouse_in (void *data, Evas * e, Evas_Object * obj,
		     void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_in");

  Evas_Event_Mouse_In *ev = (Evas_Event_Mouse_In *) event_info;
  lua_pushnumber (L, ev->output.x);
  lua_pushnumber (L, ev->output.y);
  lua_pushnumber (L, ev->canvas.x);
  lua_pushnumber (L, ev->canvas.y);

  if (lua_pcall (L, 5, 0, 0))
    printf ("lobject_cb_mouse_in: object.callback#mouse_in: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_mouse_out (void *data, Evas * e, Evas_Object * obj,
		      void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_out");

  Evas_Event_Mouse_Out *ev = (Evas_Event_Mouse_Out *) event_info;
  lua_pushnumber (L, ev->output.x);
  lua_pushnumber (L, ev->output.y);
  lua_pushnumber (L, ev->canvas.x);
  lua_pushnumber (L, ev->canvas.y);

  if (lua_pcall (L, 5, 0, 0))
    printf ("lobject_cb_mouse_out: object.callback#mouse_out: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_mouse_down (void *data, Evas * e, Evas_Object * obj,
		       void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_down");

  Evas_Event_Mouse_Down *ev = (Evas_Event_Mouse_Down *) event_info;
  lua_pushnumber (L, ev->button);
  lua_pushnumber (L, ev->output.x);
  lua_pushnumber (L, ev->output.y);
  lua_pushnumber (L, ev->canvas.x);
  lua_pushnumber (L, ev->canvas.y);

  if (lua_pcall (L, 6, 0, 0))
    printf ("lobject_cb_mouse_down: object.callback#mouse_down: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_mouse_up (void *data, Evas * e, Evas_Object * obj,
		     void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_up");

  Evas_Event_Mouse_Up *ev = (Evas_Event_Mouse_Up *) event_info;
  lua_pushnumber (L, ev->button);
  lua_pushnumber (L, ev->output.x);
  lua_pushnumber (L, ev->output.y);
  lua_pushnumber (L, ev->canvas.x);
  lua_pushnumber (L, ev->canvas.y);

  if (lua_pcall (L, 6, 0, 0))
    printf ("lobject_cb_mouse_up: object.callback#mouse_up: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_mouse_move (void *data, Evas * e, Evas_Object * obj,
		       void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_move");

  Evas_Event_Mouse_Move *ev = (Evas_Event_Mouse_Move *) event_info;
  lua_pushnumber (L, ev->buttons);
  lua_pushnumber (L, ev->cur.output.x);
  lua_pushnumber (L, ev->cur.output.y);
  lua_pushnumber (L, ev->cur.canvas.x);
  lua_pushnumber (L, ev->cur.canvas.y);
  lua_pushnumber (L, ev->prev.output.x);
  lua_pushnumber (L, ev->prev.output.y);
  lua_pushnumber (L, ev->prev.canvas.x);
  lua_pushnumber (L, ev->prev.canvas.y);

  if (lua_pcall (L, 10, 0, 0))
    printf ("lobject_cb_mouse_move: object.callback#mouse_move: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_mouse_wheel (void *data, Evas * e, Evas_Object * obj,
			void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_mouse_wheel");

  Evas_Event_Mouse_Wheel *ev = (Evas_Event_Mouse_Wheel *) event_info;
  lua_pushnumber (L, ev->z);
  lua_pushnumber (L, ev->output.x);
  lua_pushnumber (L, ev->output.y);
  lua_pushnumber (L, ev->canvas.x);
  lua_pushnumber (L, ev->canvas.y);
  if (lua_pcall (L, 6, 0, 0))
    printf ("lobject_cb_mouse_wheel: object.callback#mouse_wheel: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_free (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_free");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_free: object.callback#free: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_key_down (void *data, Evas * e, Evas_Object * obj,
		     void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_key_down");

  lua_pushstring (L, ((Evas_Event_Key_Down *) event_info)->keyname);
  if (lua_pcall (L, 2, 0, 0))
    printf ("lobject_cb_key_down: object.callback#key_down: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_key_up (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_key_up");

  lua_pushstring (L, ((Evas_Event_Key_Up *) event_info)->keyname);
  if (lua_pcall (L, 2, 0, 0))
    printf ("lobject_cb_key_up: object.callback#key_up: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_focus_in (void *data, Evas * e, Evas_Object * obj,
		     void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_focus_in");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_focus_in: object.callback#focus_in: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_focus_out (void *data, Evas * e, Evas_Object * obj,
		      void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_focus_out");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_focus_out: object.callback#focus_out: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_show (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_show");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_show: object.callback#show: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_hide (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_hide");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_hide: object.callback#hide: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_move (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_move");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_move: object.callback#move: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_resize (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_resize");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_resize: object.callback#resize: %s\n",
	    luaL_checkstring (L, -1));
}

void
lobject_cb_restack (void *data, Evas * e, Evas_Object * obj, void *event_info)
{
  lua_State *L = (lua_State *) data;
  lobject_cb_macro (L, obj, "cb_restack");

  if (lua_pcall (L, 1, 0, 0))
    printf ("lobject_cb_restack: object.callback#restack: %s\n",
	    luaL_checkstring (L, -1));
}

/*
 * object funtion methods
 */

int
lobject_fn_del (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (obj->data, "luaobj");
  luaobj_free_ref (L, ref);
  evas_object_del (obj->data);
  return 0;
}

FN (lobject_fn_raise, evas_object_raise);
FN (lobject_fn_lower, evas_object_lower);
FN_OBJ (lobject_fn_stack_above, evas_object_stack_above);
FN_OBJ (lobject_fn_stack_below, evas_object_stack_below);
FN_2INTEGER (lobject_fn_move, evas_object_move);
FN_2INTEGER (lobject_fn_resize, evas_object_resize);

int
lobject_fn_rel_move (lua_State * L)
{
  /* 1: obj
   * 2: x
	 * 3: y
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int w, h;
  Evas *evas = evas_object_evas_get (obj->data);
  evas_output_size_get (evas, &w, &h);
  int abs_x = (int) ((float) luaL_checknumber (L, 2) * (float) w);
  int abs_y = (int) ((float) luaL_checknumber (L, 3) * (float) h);
  evas_object_move (obj->data, abs_x, abs_y);
  return 0;
}

static int
lobject_fn_rel_resize (lua_State * L)
{
  /* 1: obj
   * 2: w
	 * 3: h
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int w, h;
  Evas *evas = evas_object_evas_get (obj->data);
  evas_output_size_get (evas, &w, &h);
  int abs_w = (int) ((float) luaL_checknumber (L, 2) * (float) w);
  int abs_h = (int) ((float) luaL_checknumber (L, 3) * (float) h);
  evas_object_resize (obj->data, abs_w, abs_h);
  return 0;
}

FN (lobject_fn_show, evas_object_show);
FN (lobject_fn_hide, evas_object_hide);
FN (lobject_fn_clip_unset, evas_object_clip_unset);

/*
 * object get methods
 */
GET_STRING (lobject_get_type, evas_object_type_get);
GET_INTEGER (lobject_get_layer, evas_object_layer_get);

static int
lobject_get_above (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *above = evas_object_above_get (obj->data);
  if (above != NULL)
    {
      luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (above, "luaobj");
      luaobj_get_ref (L, ref);
    }
  else
    lua_pushnil (L);
  return 1;
}

static int
lobject_get_below (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *below = evas_object_below_get (obj->data);
  if (below != NULL)
    {
      luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (below, "luaobj");
      luaobj_get_ref (L, ref);
    }
  else
    lua_pushnil (L);
  return 1;
}

GET_TAB4INTEGER (lobject_get_geometry, evas_object_geometry_get);
GET_BOOL (lobject_get_anti_alias, evas_object_anti_alias_get);
GET_BOOL (lobject_get_visible, evas_object_visible_get);
GET_INTEGER (lobject_get_render_op, evas_object_render_op_get);
GET_FLOAT (lobject_get_scale, evas_object_scale_get);
GET_TAB4INTEGER (lobject_get_color, evas_object_color_get);
GET_INTEGER (lobject_get_color_interpolation, evas_object_color_interpolation_get);

static int
lobject_get_clip (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *clip = evas_object_clip_get (obj->data);
  if (clip != NULL)
    {
      luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (clip, "luaobj");
      luaobj_get_ref (L, ref);
    }
  else
    lua_pushnil (L);
  return 1;
}

static int
lobject_get_clipees (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  const Evas_List *list = evas_object_clipees_get (obj->data);
  if (list != NULL)
    {
      Evas_List *ptr = (Evas_List *) list;
      lua_newtable (L);
      int n = 1;
      while (ptr)
	{
	  Evas_Object *item = (Evas_Object *) evas_list_data (ptr);
	  luaobj_Ref *ref =
	    (luaobj_Ref *) evas_object_data_get (item, "luaobj");
	  luaobj_get_ref (L, ref);
	  lua_rawseti (L, -2, n++);
	  ptr = evas_list_next (ptr);
	}
    }
  else
    lua_pushnil (L);
  return 1;
}

GET_STRING (lobject_get_name, evas_object_name_get);

static int
lobject_get_evas (lua_State * L)
{
  /* 1: obj
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas *evas = evas_object_evas_get (obj->data);
  luaobj_Ref *ref = (luaobj_Ref *) evas_data_attach_get (evas);
  luaobj_get_ref (L, ref);
  return 1;
}

GET_BOOL (lobject_get_focus, evas_object_focus_get);
GET_INTEGER (lobject_get_pointer_mode, evas_object_pointer_mode_get);
GET_BOOL (lobject_get_precise_is_inside, evas_object_precise_is_inside_get);
GET_BOOL (lobject_get_pass_events, evas_object_pass_events_get);
GET_BOOL (lobject_get_repeat_events, evas_object_repeat_events_get);
GET_BOOL (lobject_get_propagate_events, evas_object_propagate_events_get);
GET_TAB2INTEGER (lobject_get_size_hint_min, evas_object_size_hint_min_get);
GET_TAB2INTEGER (lobject_get_size_hint_max, evas_object_size_hint_max_get);
GET_TAB2INTEGER (lobject_get_size_hint_request, evas_object_size_hint_request_get);
GET_TAB3INTEGER (lobject_get_size_hint_aspect, evas_object_size_hint_aspect_get);
GET_TAB2FLOAT (lobject_get_size_hint_align, evas_object_size_hint_align_get);
GET_TAB2FLOAT (lobject_get_size_hint_weight, evas_object_size_hint_weight_get);
GET_TAB4INTEGER (lobject_get_size_hint_padding, evas_object_size_hint_padding_get);

/*
 * object set methods
 */
static int
lobject_set_anti_alias (lua_State * L)
{
  /* 1: obj
   * 2: antialias
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_anti_alias_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

static int
lobject_set_layer (lua_State * L)
{
  /* 1: obj
   * 2: layer
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_layer_set (obj->data, (int) luaL_checknumber (L, 2));

  return 0;
}

static int
lobject_set_render_op (lua_State * L)
{
  /* 1: obj
   * 2: render_op
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_render_op_set (obj->data, (int) luaL_checknumber (L, 2));
  return 0;
}

static int
lobject_set_scale (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_scale_set (obj->data, luaL_checknumber (L, 2));
  return 0;
}

static int
lobject_set_color (lua_State * L)
{
  /* 1: obj
   * 2: color
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);	// r
  lua_rawgeti (L, 2, 2);	// g
  lua_rawgeti (L, 2, 3);	// b
  lua_rawgeti (L, 2, 4);	// a
  evas_object_color_set (obj->data, (int) luaL_checknumber (L, -4),	// r
			 (int) luaL_checknumber (L, -3),	// g
			 (int) luaL_checknumber (L, -2),	// b
			 (int) luaL_checknumber (L, -1));	// a
  return 0;
}

static int
lobject_set_color_interpolation (lua_State * L)
{
  /* 1: obj
   * 2: color_interpolation
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_color_interpolation_set (obj->data, (int) luaL_checknumber (L, 2));
  return 0;
}

static int
lobject_set_clip (lua_State * L)
{
  /* 1: obj
   * 2: clip
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Object *clip = (luaobj_Object *) lua_touserdata (L, 2);
  evas_object_clip_set (obj->data, clip->data);
  return 0;
}

static int
lobject_set_name (lua_State * L)
{
  /* 1: obj
   * 2: name
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_name_set (obj->data, luaL_checkstring (L, 2));
  return 0;
}

static int
lobject_set_focus (lua_State * L)
{
  /* 1: obj
   * 2: focus
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_focus_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

static int
lobject_set_pointer_mode (lua_State * L)
{
  /* 1: obj
   * 2: focus
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_pointer_mode_set (obj->data, lua_tonumber (L, 2));
  return 0;
}

static int
lobject_set_precise_is_inside (lua_State * L)
{
  /* 1: obj
   * 2: focus
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_precise_is_inside_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

static int
lobject_set_pass_events (lua_State * L)
{
  /* 1: obj
   * 2: pass_events
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_pass_events_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

static int
lobject_set_repeat_events (lua_State * L)
{
  /* 1: obj
   * 2: repeat_events
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_repeat_events_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

static int
lobject_set_propagate_events (lua_State * L)
{
  /* 1: obj
   * 2: propagate_events
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_propagate_events_set (obj->data, lua_toboolean (L, 2));
  return 0;
}

//---------callbacks------------------------------------------------------------//
void
lobject_set_callback_call (lua_State * L, Evas_Object * obj, const char *key,
			   Evas_Callback_Type type, void (*func) (void *,
								  Evas *,
								  Evas_Object
								  *, void *))
{
  /* 1: object
   * 2: function
   */
  if (!lua_isnil (L, 2))
    {				// callback_add
      evas_object_event_callback_add (obj, type, func, L);
      evas_object_data_set (obj, key, luaobj_new_ref (L, -1));
    }
  else
    {				// callback_del
      evas_object_event_callback_del (obj, type, func);
      luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (obj, key);
      evas_object_data_set (obj, key, NULL);
      luaobj_free_ref (L, ref);
    }
}

static int
lobject_set_callback_mouse_in (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_in",
			     EVAS_CALLBACK_MOUSE_IN, lobject_cb_mouse_in);
  return 0;
}

static int
lobject_set_callback_mouse_out (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_out",
			     EVAS_CALLBACK_MOUSE_OUT, lobject_cb_mouse_out);
  return 0;
}

static int
lobject_set_callback_mouse_down (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_down",
			     EVAS_CALLBACK_MOUSE_DOWN, lobject_cb_mouse_down);
  return 0;
}
static int
lobject_set_callback_mouse_up (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_up",
			     EVAS_CALLBACK_MOUSE_UP, lobject_cb_mouse_up);
  return 0;
}
static int
lobject_set_callback_mouse_move (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_move",
			     EVAS_CALLBACK_MOUSE_MOVE, lobject_cb_mouse_move);
  return 0;
}

static int
lobject_set_callback_mouse_wheel (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_mouse_wheel",
			     EVAS_CALLBACK_MOUSE_WHEEL,
			     lobject_cb_mouse_wheel);
  return 0;
}

static int
lobject_set_callback_free (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_free",
			     EVAS_CALLBACK_FREE, lobject_cb_free);
  return 0;
}

static int
lobject_set_callback_key_down (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_key_down",
			     EVAS_CALLBACK_KEY_DOWN, lobject_cb_key_down);
  return 0;
}

static int
lobject_set_callback_key_up (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_key_up",
			     EVAS_CALLBACK_KEY_UP, lobject_cb_key_up);
  return 0;
}

static int
lobject_set_callback_focus_in (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_focus_in",
			     EVAS_CALLBACK_FOCUS_IN, lobject_cb_focus_in);
  return 0;
}

static int
lobject_set_callback_focus_out (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_focus_out",
			     EVAS_CALLBACK_FOCUS_OUT, lobject_cb_focus_out);
  return 0;
}

static int
lobject_set_callback_show (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_show",
			     EVAS_CALLBACK_SHOW, lobject_cb_show);
  return 0;
}

static int
lobject_set_callback_hide (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_hide",
			     EVAS_CALLBACK_HIDE, lobject_cb_hide);
  return 0;
}

static int
lobject_set_callback_resize (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_resize",
			     EVAS_CALLBACK_RESIZE, lobject_cb_resize);
  return 0;
}

static int
lobject_set_callback_move (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_move",
			     EVAS_CALLBACK_MOVE, lobject_cb_move);
  return 0;
}

static int
lobject_set_callback_restack (lua_State * L)
{
  /* 1: object
   * 2: function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lobject_set_callback_call (L, obj->data, "cb_restack",
			     EVAS_CALLBACK_RESTACK, lobject_cb_restack);
  return 0;
}

static int
lobject_set_visible (lua_State *L)
{
	/* 1: object
	 * 2: bool
	 */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	int visible = lua_toboolean (L, 2);
	if (visible)
		evas_object_show (obj->data);
	else
		evas_object_hide (obj->data);
	return 0;
}

static int
lobject_set_pos (lua_State *L)
{
	/* 1: object
	 * 2: table
	 */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
  Evas *evas = evas_object_evas_get (obj->data);
	int w, h;
  evas_output_size_get (evas, &w, &h);
  int abs_x = (int) ((float) luaL_checknumber (L, -2) * (float) w);
  int abs_y = (int) ((float) luaL_checknumber (L, -1) * (float) h);
  evas_object_move (obj->data, abs_x, abs_y);
	return 0;
}

static int
lobject_set_size (lua_State *L)
{
	/* 1: object
	 * 2: table
	 */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
  Evas *evas = evas_object_evas_get (obj->data);
	int w, h;
  evas_output_size_get (evas, &w, &h);
  int abs_w = (int) ((float) luaL_checknumber (L, -2) * (float) w);
  int abs_h = (int) ((float) luaL_checknumber (L, -1) * (float) h);
  evas_object_resize (obj->data, abs_w, abs_h);
	return 0;
}

static int
lobject_set_size_hint_min (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	evas_object_size_hint_min_set (
		obj->data,
		luaL_checkint (L, -2),
		luaL_checkint (L, -1));
	return 0;
}

static int
lobject_set_size_hint_max (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	evas_object_size_hint_max_set (
		obj->data,
		luaL_checkint (L, -2),
		luaL_checkint (L, -1));
	return 0;
}

static int
lobject_set_size_hint_request (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	evas_object_size_hint_request_set (
		obj->data,
		luaL_checkint (L, -2),
		luaL_checkint (L, -1));
	return 0;
}

static int
lobject_set_size_hint_aspect (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	lua_rawgeti (L, 2, 3);
	evas_object_size_hint_aspect_set (
		obj->data,
		luaL_checkint (L, -3),
		luaL_checkint (L, -2),
		luaL_checkint (L, -1));
	return 0;
}

static int
lobject_set_size_hint_align (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	evas_object_size_hint_align_set (
		obj->data,
		luaL_checknumber (L, -2),
		luaL_checknumber (L, -1));
	return 0;
}

static int
lobject_set_size_hint_weight (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	evas_object_size_hint_weight_set (
		obj->data,
		luaL_checknumber (L, -2),
		luaL_checknumber (L, -1));
	return 0;
}

static int
lobject_set_size_hint_padding (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1);
	lua_rawgeti (L, 2, 2);
	lua_rawgeti (L, 2, 3);
	lua_rawgeti (L, 2, 4);
	evas_object_size_hint_padding_set (
		obj->data,
		luaL_checkint (L, -4),
		luaL_checkint (L, -3),
		luaL_checkint (L, -2),
		luaL_checkint (L, -1));
	return 0;
}

/*
 * object function definitions
 */
const struct luaL_Reg lObject_fn[] = {
  {"del", lobject_fn_del},
  {"raise", lobject_fn_raise},
  {"lower", lobject_fn_lower},
  {"stack_above", lobject_fn_stack_above},
  {"stack_below", lobject_fn_stack_below},

  {"move", lobject_fn_move},
  {"rel_move", lobject_fn_rel_move},
  {"resize", lobject_fn_resize},
  {"rel_resize", lobject_fn_rel_resize},

  {"show", lobject_fn_show},
  {"hide", lobject_fn_hide},

  {"clip_unset", lobject_fn_clip_unset},
  {NULL, NULL}			// sentinel
};

/*
 * object get definitions
 */
const struct luaL_Reg lObject_get[] = {
  {"type", lobject_get_type},
  {"layer", lobject_get_layer},
  {"above", lobject_get_above},
  {"below", lobject_get_below},
  {"geometry", lobject_get_geometry},

	{"size_hint_min", lobject_get_size_hint_min},
	{"size_hint_max", lobject_get_size_hint_max},
	{"size_hint_request", lobject_get_size_hint_request},
	{"size_hint_aspect", lobject_get_size_hint_aspect},
	{"size_hint_align", lobject_get_size_hint_align},
	{"size_hint_weight", lobject_get_size_hint_weight},
	{"size_hint_padding", lobject_get_size_hint_padding},

  {"visible", lobject_get_visible},
  {"anti_alias", lobject_get_anti_alias},
  {"render_op", lobject_get_render_op},
	{"scale", lobject_get_scale},
  {"color", lobject_get_color},
  {"color_interpolation", lobject_get_color_interpolation},
  {"clip", lobject_get_clip},
  {"clipees", lobject_get_clipees},
  {"name", lobject_get_name},

  {"evas", lobject_get_evas},

  {"focus", lobject_get_focus},
	{"pointer_mode", lobject_get_pointer_mode},
	{"precise_is_inside", lobject_get_precise_is_inside},
  {"pass_events", lobject_get_pass_events},
  {"repeat_events", lobject_get_repeat_events},
  {"propagate_events", lobject_get_propagate_events},
  {NULL, NULL}			// sentinel
};

/*
 * object set definitions
 */
const struct luaL_Reg lObject_set[] = {
	{"size_hint_min", lobject_set_size_hint_min},
	{"size_hint_max", lobject_set_size_hint_max},
	{"size_hint_request", lobject_set_size_hint_request},
	{"size_hint_aspect", lobject_set_size_hint_aspect},
	{"size_hint_align", lobject_set_size_hint_align},
	{"size_hint_weight", lobject_set_size_hint_weight},
	{"size_hint_padding", lobject_set_size_hint_padding},

  {"layer", lobject_set_layer},
  {"render_op", lobject_set_render_op},
	{"scale", lobject_set_scale},
  {"anti_alias", lobject_set_anti_alias},
  {"color", lobject_set_color},
  {"color_interpolation", lobject_set_color_interpolation},
  {"clip", lobject_set_clip},
  {"name", lobject_set_name},
  {"focus", lobject_set_focus},
	{"pointer_mode", lobject_set_pointer_mode},
	{"precise_is_inside", lobject_set_precise_is_inside},
  {"pass_events", lobject_set_pass_events},
  {"repeat_events", lobject_set_repeat_events},
  {"propagate_events", lobject_set_propagate_events},

  {"callback_mouse_in", lobject_set_callback_mouse_in},
  {"callback_mouse_out", lobject_set_callback_mouse_out},
  {"callback_mouse_down", lobject_set_callback_mouse_down},
  {"callback_mouse_up", lobject_set_callback_mouse_up},
  {"callback_mouse_move", lobject_set_callback_mouse_move},
  {"callback_mouse_wheel", lobject_set_callback_mouse_wheel},
  {"callback_free", lobject_set_callback_free},
  {"callback_key_down", lobject_set_callback_key_down},
  {"callback_key_up", lobject_set_callback_key_up},
  {"callback_focus_in", lobject_set_callback_focus_in},
  {"callback_focus_out", lobject_set_callback_focus_out},
  {"callback_show", lobject_set_callback_show},
  {"callback_hide", lobject_set_callback_hide},
  {"callback_move", lobject_set_callback_move},
  {"callback_resize", lobject_set_callback_resize},
  {"callback_restack", lobject_set_callback_restack},

	{"visible", lobject_set_visible},
	{"pos", lobject_set_pos},
	{"size", lobject_set_size},

  {NULL, NULL}			// sentinel
};

/*
 * object definitions
 */

const luaL_Reg lObject_mt [] = {
	{"__gc", lobject_fn_del},
	{NULL, NULL}
};

const luaobj_Reg mObject = {
  lObject_mt,
  lObject_get,
  lObject_set,
  lObject_fn
};

const luaobj_Reg *cObject [] = {
  &mClass,
  &mObject,
  NULL
};

const luaobj_Reg *cRect [] = {
	&mClass,
	&mObject,
	NULL // sentinel
};

