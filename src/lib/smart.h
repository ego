#ifndef EGO_EVAS_SMART_H
#define EGO_EVAS_SMART_H

#include <Evas.h>

extern const luaobj_Reg *cSmart[];
extern luaobj_Ref *rSmart;

Evas_Smart *lsmart_class_new (void *);

#endif
