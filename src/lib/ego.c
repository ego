#include "ego.h"

#include <lua.h>
#include <Evas.h>

#include "luaobj.h"
#include "class.h"
#include "evas.h"
#include "object.h"
#include "image.h"
#include "animator.h"
#include "smart.h"
#include "edje.h"
#include "part.h"
#include "line.h"
#include "polygon.h"
#include "gradient.h"
#include "gradient2.h"
#include "text.h"
#include "textblock.h"
#include "textblock_style.h"
#include "table.h"
#include "box.h"
#include "elementary.h"
#include "transform.h"

void
luaopen_ego (lua_State *L)
{
	// register classes
	luaobj_new_class (L, "cEgo", cEgo);
	luaobj_new_class (L, "cObject", cObject);
	luaobj_new_class (L, "cRect", cRect);
	luaobj_new_class (L, "cImage", cImage);
	luaobj_new_class (L, "cAnimator", cAnimator);
	luaobj_new_class (L, "cSmart", cSmart);
	luaobj_new_class (L, "cEdje", cEdje);
	luaobj_new_class (L, "cPart", cPart);
	luaobj_new_class (L, "cLine", cLine);
	luaobj_new_class (L, "cPolygon", cPolygon);
	luaobj_new_class (L, "cGradient", cGradient);
	luaobj_new_class (L, "cGradient2_Linear", cGradient2_Linear);
	luaobj_new_class (L, "cGradient2_Radial", cGradient2_Radial);
	luaobj_new_class (L, "cText", cText);
	luaobj_new_class (L, "cTextblock", cTextblock);
	luaobj_new_class (L, "cStyle", cStyle);
	luaobj_new_class (L, "cTransform", cTransform);
	luaobj_new_class (L, "cTable", cTable);
	luaobj_new_class (L, "cBox", cBox);

	luaobj_new_class (L, "cIcon", cIcon);
	luaobj_new_class (L, "cBox2", cBox2);
	luaobj_new_class (L, "cButton", cButton);
	luaobj_new_class (L, "cScroller", cScroller);
	luaobj_new_class (L, "cLabel", cLabel);
	luaobj_new_class (L, "cToggle", cToggle);
	luaobj_new_class (L, "cFrame", cFrame);
	luaobj_new_class (L, "cTable2", cTable2);
	luaobj_new_class (L, "cEntry", cEntry);
	luaobj_new_class (L, "cBubble", cBubble);
	luaobj_new_class (L, "cHover", cHover);
	luaobj_new_class (L, "cLayout", cLayout);
	luaobj_new_class (L, "cClock", cClock);
}

void
ego_sandbox (lua_State *L, int level)
{
	switch (level)
	{
		case 0:
			/*
			 *access to io and os routines
			 */
			luaL_openlibs (L);
			break;
		case 1:
			/*
			 * no access to io and os routines
			 */
			luaopen_base (L);
			luaopen_table (L);
			luaopen_string (L);
			luaopen_math (L);
			break;
		case 2:
			/*
			 * no access to io and os routines
			 * no loading and execution of script files
			 */
			luaopen_base (L);
			luaopen_table (L);
			luaopen_string (L);
			luaopen_math (L);
			lua_pushnil (L);
				lua_setglobal (L, "loadfile");
			lua_pushnil (L);
				lua_setglobal (L, "dofile");
			break;
		case 3:
			/*
			 * no access to io and os routines
			 * no loading and execution of script files
			 * no loading and execution of script strings
			 */
			luaopen_base (L);
			luaopen_table (L);
			luaopen_string (L);
			luaopen_math (L);
			lua_pushnil (L);
				lua_setglobal (L, "loadfile");
			lua_pushnil (L);
				lua_setglobal (L, "dofile");
			lua_pushnil (L);
				lua_setglobal (L, "loadstring");
			lua_pushnil (L);
				lua_setglobal (L, "dostring");
			break;
		case 4:
			/*
			 * no access to io and os routines
			 * no loading and execution of script files
			 * no loading and execution of script strings
			 * no manipulation of environmental variables
			 */
			luaopen_base (L);
			luaopen_table (L);
			luaopen_string (L);
			luaopen_math (L);
			lua_pushnil (L);
				lua_setglobal (L, "loadfile");
			lua_pushnil (L);
				lua_setglobal (L, "dofile");
			lua_pushnil (L);
				lua_setglobal (L, "loadstring");
			lua_pushnil (L);
				lua_setglobal (L, "dostring");
			lua_pushnil (L);
				lua_setglobal (L, "getfenv");
			lua_pushnil (L);
				lua_setglobal (L, "setfenv");
			break;
	}
}

/*
 * ego smart class implementatoin
 */
static lua_State *glob_L = NULL;
static Evas_Smart_Class parent_sc = {NULL};
static Evas_Smart_Class sc = {NULL};

static void
ego_smart_class_add (Evas_Object *obj)
{
	lua_State *L = glob_L;
	parent_sc.add (obj);
	
	// register main class
	luaobj_Object *lobj = lua_newuserdata (L, sizeof (luaobj_Object));
	luaobj_set_class (L, -1, lobj, "cEgo");
	lobj->data = obj;
	evas_object_data_set (lobj->data, "luaobj", luaobj_new_ref (L, -1));
	lua_setglobal (L, "ego");

	// read in buffer at index -1
	if (lua_pcall (L, 0, LUA_MULTRET, 0))
	{
		fprintf (stderr, "ego parse error: %s\n", lua_tostring (L, -1));
		exit (-1);
	}
}

static Evas_Smart *
ego_smart_class_new (void)
{
	if (!parent_sc.name)
	{
		parent_sc.name = "parent_ego";
		parent_sc.version = EVAS_SMART_CLASS_VERSION;
		evas_object_smart_clipped_smart_set (&parent_sc);
	}
	if (!sc.name)
	{
		sc.name = "ego";
		sc.version = EVAS_SMART_CLASS_VERSION;
		evas_object_smart_clipped_smart_set (&sc);
		sc.add = ego_smart_class_add;
	}
	return evas_smart_class_new (&sc);
}

/*
 * ego smart class creation
 */
Evas_Object *
ego_object_add (Evas *evas, lua_State *L)
{
	// create smart object
	glob_L = L;

	return evas_object_smart_add (evas, ego_smart_class_new ());
}

