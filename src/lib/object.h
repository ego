#ifndef EGO_OBJECT_H
#define EGO_OBJECT_H

#include "luaobj.h"

extern const luaobj_Reg mObject;
extern const luaobj_Reg *cObject [];
extern const luaobj_Reg *cRect [];

#endif /* EGO_OBJECT_H */

