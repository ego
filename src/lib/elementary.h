#ifndef EGO_ELEMENTARY_H
#define EGO_ELEMENTARY_H

#include <Evas.h>
#include "luaobj.h"

extern const luaobj_Reg *cIcon[];
extern const luaobj_Reg *cBox2[];
extern const luaobj_Reg *cButton[];
extern const luaobj_Reg *cScroller[];
extern const luaobj_Reg *cLabel[];
extern const luaobj_Reg *cToggle[];
extern const luaobj_Reg *cFrame[];
extern const luaobj_Reg *cTable2[];
extern const luaobj_Reg *cClock[];
extern const luaobj_Reg *cLayout[];
extern const luaobj_Reg *cHover[];
extern const luaobj_Reg *cEntry[];
/*
extern const luaobj_Reg *cNotepad[];
extern const luaobj_Reg *cAnchorview[];
extern const luaobj_Reg *cAnchorblock[];
*/
extern const luaobj_Reg *cBubble[];

#endif
