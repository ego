#ifndef EGO_LUAOBJ_H
#define EGO_LUAOBJ_H

#include <stdlib.h>		// malloc

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

//---------------------structs------------------------------------------//

typedef struct _luaobj_Ref luaobj_Ref;
typedef struct _luaobj_Reg luaobj_Reg;
typedef struct _luaobj_Object luaobj_Object;
typedef struct _luaobj_Enum luaobj_Enum;

struct _luaobj_Ref {
  int id;
  lua_State *L;
};

struct _luaobj_Reg {
  const struct luaL_Reg *mt, *get, *set, *fn;
};

struct _luaobj_Object {
  void *data, *datb, *datc;
  lua_State *L;
  const char *type;
};

struct _luaobj_Enum {
  const char *key;
  int val;
};

//---------------------functions----------------------------------------//

// enum
void luaobj_new_enum (lua_State * L, int index, const char *name, const luaobj_Enum *enu);

// ref
luaobj_Ref *luaobj_new_ref (lua_State * L, int index);
void luaobj_get_ref (lua_State * L, luaobj_Ref * ref);
void luaobj_free_ref (lua_State * L, luaobj_Ref * ref);

// class
void luaobj_new_class (lua_State * L, char *id, const luaobj_Reg **class);
void luaobj_set_class (lua_State * L, int index, luaobj_Object *obj, const char *id);

// look
int luaobj_look_fn (lua_State * L, const char *id, const char *key);
int luaobj_look_get (lua_State * L, const char *id, const char *key);
int luaobj_look_set (lua_State * L, const char *id);

// args
void luaobj_check_args (lua_State * L, int num, ...);

// args
int luaobj_int_get (lua_State * L, int (*func) (void *obj));
int luaobj_double_get (lua_State * L, double (*func) (void *obj));
int luaobj_string_get (lua_State * L, char *(*func) (void *obj));
int luaobj_bool_get (lua_State * L, int (*func) (void *obj));

#endif /* EGO_LUAOBJ_H */

