#include "class.h"

/*
 * class metatable methods
 */

static int
lclass_mt_index (lua_State * L)
{
  /* 1: obj
   * 2: key
   */

	if (lua_isstring (L, 2))
	{
	  const char *key = luaL_checkstring (L, 2);
	  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  	if (!luaobj_look_fn (L, obj->type, key))	// look in lClass_fn
   		if (!luaobj_look_get (L, obj->type, key))
	   	{				// look in lClass_get
				lua_pushvalue (L, 1);	// look in obj ref tab
				lua_gettable (L, LUA_REGISTRYINDEX);
				lua_pushvalue (L, 2);	// key
				lua_gettable (L, -2);
		   }
	} else
	{
		lua_pushvalue (L, 1);
		lua_gettable (L, LUA_REGISTRYINDEX);
		lua_pushvalue (L, 2); // key
		lua_gettable (L, -2);
	}
	
  return 1;
}

static int
lclass_mt_newindex (lua_State * L)
{
  /* 1: obj
   * 2: key
   * 3: value
   */

	if (lua_isstring (L, 2))
	{
	  //const char *key = luaL_checkstring (L, 2);
	  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	  if (!luaobj_look_set (L, obj->type))
	  {				// look in lClass_set
	     lua_pushvalue (L, 1);	// look in obj ref tab
	     lua_gettable (L, LUA_REGISTRYINDEX);
	     lua_insert (L, -3);
	     lua_settable (L, -3);
    }
	} else
	{
		lua_pushvalue (L, 1);
		lua_gettable (L, LUA_REGISTRYINDEX);
		lua_insert (L, -3);
		lua_settable (L, -3);
	}
  return 0;
}

/*
 * class function methods
 */

static int
lclass_fn_set (lua_State * L)
{
  /* 2: table
   * 1: obj
   */

  /*
   * for k,v in pairs(table) do
   *   obj[k] = v
   * end
   */

  lua_pushnil (L);
  while (lua_next (L, -2))
    {
      // key at -2, value at -1
      lua_pushvalue (L, -2);
      lua_insert (L, -2);
      lua_settable (L, 1);
    }

  return 0;
}

static int
lclass_fn_get (lua_State * L)
{
  /* 1: obj
   */

  /*
   * res = {}
   * mtG = getmetatable(obj)['.get']
   * for k,v in pairs(mtG) do
   *   res[k] = obj[k]
   * end
   */

  lua_newtable (L);		// res
  lua_getmetatable (L, 1);	// mt
  lua_getfield (L, -1, ".get");
  lua_remove (L, -2);		// mt

  lua_pushnil (L);
  while (lua_next (L, -2))
    {
      // key at -2, value at -1
      lua_pop (L, 1);		// value = cfunction
      lua_pushvalue (L, -1);	// key
      lua_pushvalue (L, -1);	// key
      lua_gettable (L, 1);	// obj[key]
      lua_settable (L, 2);	// res[key]
    }
  lua_pop (L, 1);		// .get

  return 1;
}

static int
lclass_itr_call (lua_State * L, const char *id)
{
  /* 1: obj
   */
  lua_getmetatable (L, 1);	// mt
  lua_getfield (L, -1, id);
  lua_remove (L, -2);		// mt

  lua_getglobal (L, "pairs");
  lua_insert (L, -2);
  if (lua_pcall (L, 1, 3, 0))
    printf ("lclass_fn_tpairs: %s\n", luaL_checkstring (L, -1));
  return 3;
}

static int
lclass_fn_gpairs (lua_State * L)
{
  /* 1: obj
   */
  return lclass_itr_call (L, ".get");
}

static int
lclass_fn_spairs (lua_State * L)
{
  /* 1: obj
   */
  return lclass_itr_call (L, ".set");
}

static int
lclass_fn_fpairs (lua_State * L)
{
  /* 1: obj
   */
  return lclass_itr_call (L, ".fn");
}

static int
lclass_fn_pairs (lua_State * L)
{
  /* 1: obj
   */
  lua_gettable (L, LUA_REGISTRYINDEX);
  lua_getglobal (L, "pairs");
  lua_insert (L, -2);
  if (lua_pcall (L, 1, 3, 0))
    printf ("lclass_fn_tpairs: %s\n", luaL_checkstring (L, -1));
  return 3;
}

static int
lclass_fn_ipairs (lua_State * L)
{
  /* 1: obj
   */
  lua_gettable (L, LUA_REGISTRYINDEX);
  lua_getglobal (L, "ipairs");
  lua_insert (L, -2);
  if (lua_pcall (L, 1, 3, 0))
    printf ("lclass_fn_ipairs: %s\n", luaL_checkstring (L, -1));
  return 3;
}

/*
 * class metatable definitions
 */

const luaL_Reg lClass_mt[] = {
  {"__index", lclass_mt_index},
  {"__newindex", lclass_mt_newindex},
  {NULL, NULL}			// sentinel
};

/* 
 * class function definitions
 */

const luaL_Reg lClass_fn[] = {
  {"get", lclass_fn_get},
  {"set", lclass_fn_set},

  {"gpairs", lclass_fn_gpairs},
  {"spairs", lclass_fn_spairs},
  {"fpairs", lclass_fn_fpairs},
  {"pairs", lclass_fn_pairs},
  {"ipairs", lclass_fn_ipairs},

  {NULL, NULL}			// sentinel
};

const luaL_Reg lClass_nil[] = {
	{NULL, NULL} // sentinel
};

/*
 * class definition
 */

const luaobj_Reg mClass = {
	lClass_mt,
	lClass_nil,
	lClass_nil,
	lClass_fn
};

