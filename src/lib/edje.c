#include "edje.h"
#include "part.h"
#include "class.h"
#include "object.h"
#include "macro.h"

static int
ledje_mt_index (lua_State * L)
{
  /* 1: edje
   * 2: key
   */
  const char *key = luaL_checkstring (L, 2);
  luaobj_Object *edje = (luaobj_Object *) lua_touserdata (L, 1);

  if (!luaobj_look_fn (L, edje->type, key))
    {				// look in cEdje_fn
      if (!luaobj_look_get (L, edje->type, key))
	{			// look in cEdje_get
	  if (edje_object_part_exists (edje->data, key))
	    {
	      // check if lua userdata exists
	      luaobj_Ref *ref =
		(luaobj_Ref *) evas_object_data_get (edje->data, key);
	      if (ref)
		{		// exists
		  luaobj_get_ref (L, ref);
		}
	      else
		{		// create it
		  luaobj_Object *obj =
		    (luaobj_Object *) lua_newuserdata (L, sizeof (luaobj_Object));
		  obj->data = edje_object_part_object_get (edje->data, key);
		  evas_object_data_set (obj->data, "key", key);
		  evas_object_data_set (obj->data, "edje", edje->data);
		  luaobj_set_class (L, -1, obj, "cPart");
		  ref = luaobj_new_ref (L, -1);
		  evas_object_data_set (obj->data, "luaobj", ref);
		  evas_object_data_set (edje->data, key, ref);
		}
	    }
	  else
	    {			// look in obj reg tab
	      lua_pushvalue (L, 1);	// push edje
	      lua_gettable (L, LUA_REGISTRYINDEX);
	      lua_pushvalue (L, 2);	// key
	      lua_gettable (L, -2);
	    }
	}
    }
  return 1;
}

FN (ledje_fn_freeze, edje_object_freeze);
FN (ledje_fn_thaw, edje_object_thaw);

static int
ledje_fn_color_class_set (lua_State * L)
{
  /* 1: edje
   * 2: class
   * 3: col1
   * 4: col2
   * 5: col3
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 3, 1);
  lua_rawgeti (L, 3, 2);
  lua_rawgeti (L, 3, 3);
  lua_rawgeti (L, 3, 4);
  lua_rawgeti (L, 4, 1);
  lua_rawgeti (L, 4, 2);
  lua_rawgeti (L, 4, 3);
  lua_rawgeti (L, 4, 4);
  lua_rawgeti (L, 5, 1);
  lua_rawgeti (L, 5, 2);
  lua_rawgeti (L, 5, 3);
  lua_rawgeti (L, 5, 4);
  edje_object_color_class_set (obj->data,
			       luaL_checkstring (L, 2),
			       (int) luaL_checknumber (L, -12),
			       (int) luaL_checknumber (L, -11),
			       (int) luaL_checknumber (L, -10),
			       (int) luaL_checknumber (L, -9),
			       (int) luaL_checknumber (L, -8),
			       (int) luaL_checknumber (L, -7),
			       (int) luaL_checknumber (L, -6),
			       (int) luaL_checknumber (L, -5),
			       (int) luaL_checknumber (L, -4),
			       (int) luaL_checknumber (L, -3),
			       (int) luaL_checknumber (L, -2),
			       (int) luaL_checknumber (L, -1));
  return 0;
}

FN_STRING (ledje_fn_color_class_del, edje_object_color_class_del);

static int
ledje_fn_text_class_set (lua_State * L)
{
  /* 1: edje
   * 2: class
   * 3: font
   * 4: size
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  edje_object_text_class_set (obj->data,
			      luaL_checkstring (L, 2),
			      luaL_checkstring (L, 3), (int) luaL_checknumber (L, 4));
  return 0;
}

FN_2STRING (ledje_fn_signal_emit, edje_object_signal_emit);

static int
ledje_get_group (lua_State * L)
{
  /* 1: edje
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  const char *file, *group;
  edje_object_file_get (obj->data, &file, &group);
  lua_pushstring (L, group);
  return 1;
}

GET_INTEGER (ledje_get_load_error, edje_object_load_error_get);
GET_BOOL (ledje_get_play, edje_object_play_get);
GET_BOOL (ledje_get_animation, edje_object_animation_get);
GET_TAB2INTEGER (ledje_get_size_min, edje_object_size_min_get);
GET_TAB2INTEGER (ledje_get_size_max, edje_object_size_max_get);
GET_FLOAT (ledje_get_frametime, edje_frametime_get);

static void
ledje_cb (void *data,
	  Evas_Object * edj, const char *signal, const char *source)
{
  lua_State *L = (lua_State *) data;

  luaobj_Ref *obj = (luaobj_Ref *) evas_object_data_get (edj, "luaobj");
  luaobj_Ref *cb = (luaobj_Ref *) evas_object_data_get (edj, "signal_cb");

  luaobj_get_ref (L, cb);		// signal callback function

  if (lua_isfunction (L, -1))
    {
      luaobj_get_ref (L, obj);	// edje
      lua_pushstring (L, signal);	// signal
      lua_pushstring (L, source);	// source

      if (lua_pcall (L, 3, 0, 0))
	printf ("edje_cb: %s|%s: %s\n", signal, source, luaL_checkstring (L, -1));
    }
}

static int
ledje_set_group (lua_State * L)
{
  /* 2: tab
   * 1: group
   */

  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);

  const char *group = luaL_checkstring (L, 2);
	lua_getfield (L, LUA_REGISTRYINDEX, "ego_file");
	const char *file = luaL_checkstring (L, -1);

  edje_object_file_set (obj->data, file, group);

  return 0;
}

SET_FLOAT (ledje_set_frametime, edje_frametime_set);
SET_BOOL (ledje_set_play, edje_object_play_set);
SET_BOOL (ledje_set_animation, edje_object_animation_set);

static int
ledje_set_signal_callback (lua_State * L)
{
  /* 1: edje
   * 2: callback function
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  if (!lua_isnil (L, 2))
    {
      edje_object_signal_callback_add (obj->data, "*", "*", ledje_cb,
				       (void *) L);
      evas_object_data_set (obj->data, "signal_cb", luaobj_new_ref (L, -1));
    }
  else
    {
      edje_object_signal_callback_del (obj->data, "*", "*", ledje_cb);
      luaobj_Ref *ref =
	(luaobj_Ref *) evas_object_data_get (obj->data, "signal_cb");
      evas_object_data_set (obj->data, "signal_cb", NULL);
      luaobj_free_ref (L, ref);
    }
  return 0;
}

const struct luaL_Reg lEdje_mt[] = {
  {"__index", ledje_mt_index},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lEdje_fn[] = {
  {"freeze", ledje_fn_freeze},
  {"thaw", ledje_fn_thaw},
  {"color_class_set", ledje_fn_color_class_set},
  {"color_class_del", ledje_fn_color_class_del},

  {"text_class_set", ledje_fn_text_class_set},

  {"signal_emit", ledje_fn_signal_emit},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lEdje_get[] = {
  {"group", ledje_get_group},
  {"load_error", ledje_get_load_error},
  {"play", ledje_get_play},
  {"animation", ledje_get_animation},

  {"size_in", ledje_get_size_min},
  {"size_max", ledje_get_size_max},

  {"frametime", ledje_get_frametime},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lEdje_set[] = {
  {"group", ledje_set_group},
  {"frametime", ledje_set_frametime},
  {"play", ledje_set_play},
  {"animation", ledje_set_animation},
  {"signal_callback", ledje_set_signal_callback},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mEdje = {
  lEdje_mt,
  lEdje_get,
  lEdje_set,
  lEdje_fn
};

const luaobj_Reg *cEdje[] = {
  &mClass,
  &mObject,
  &mEdje,
  NULL
};

