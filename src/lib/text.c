#include "text.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"

GET_STRING (ltext_get_font_source, evas_object_text_font_source_get);

static int
ltext_get_font (lua_State * L)
{
  /* 1: text
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  const char *font;
  int size;
  evas_object_text_font_get (obj->data, &font, &size);
  lua_newtable (L);
  lua_pushstring (L, font);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, size);
  lua_rawseti (L, -2, 2);
  return 1;
}

GET_STRING (ltext_get_text, evas_object_text_text_get);
GET_INTEGER (ltext_get_ascent, evas_object_text_ascent_get);
GET_INTEGER (ltext_get_descent, evas_object_text_descent_get);
GET_INTEGER (ltext_get_max_ascent, evas_object_text_ascent_get);
GET_INTEGER (ltext_get_max_descent, evas_object_text_descent_get);
GET_INTEGER (ltext_get_horiz_advance, evas_object_text_horiz_advance_get);
GET_INTEGER (ltext_get_vert_advance, evas_object_text_vert_advance_get);
GET_INTEGER (ltext_get_inset, evas_object_text_inset_get);

// FIXME // to implement // char_pos_get // char_coords_get

GET_INTEGER (ltext_get_style, evas_object_text_style_get);
GET_TAB3INTEGER (ltext_get_shadow_color, evas_object_text_shadow_color_get);
GET_TAB3INTEGER (ltext_get_glow_color, evas_object_text_glow_color_get);
GET_TAB3INTEGER (ltext_get_glow2_color, evas_object_text_glow2_color_get);
GET_TAB3INTEGER (ltext_get_outline_color, evas_object_text_outline_color_get);
GET_TAB4INTEGER (ltext_get_style_pad, evas_object_text_style_pad_get);

SET_STRING (ltext_set_font_source, evas_object_text_font_source_set);

static int
ltext_set_font (lua_State * L)
{
  /* 1: text
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);	// font
  lua_rawgeti (L, 2, 2);	// size
  evas_object_text_font_set (obj->data,
			     luaL_checkstring (L, -2),
			     (int) luaL_checknumber (L, -1));
  return 0;
}

SET_STRING (ltext_set_text, evas_object_text_text_set);
SET_INTEGER (ltext_set_style, evas_object_text_style_set);
SET_TAB3INTEGER (ltext_set_shadow_color, evas_object_text_shadow_color_set);
SET_TAB3INTEGER (ltext_set_glow_color, evas_object_text_glow_color_set);
SET_TAB3INTEGER (ltext_set_glow2_color, evas_object_text_glow2_color_set);
SET_TAB3INTEGER (ltext_set_outline_color, evas_object_text_outline_color_set);

const struct luaL_Reg lText_get[] = {
  {"font_source", ltext_get_font_source},
  {"font", ltext_get_font},
  {"text", ltext_get_text},

  {"ascent", ltext_get_ascent},
  {"descent", ltext_get_descent},
  {"max_ascent", ltext_get_max_ascent},
  {"max_descent", ltext_get_max_descent},
  {"horiz_advance", ltext_get_horiz_advance},
  {"vert_advance", ltext_get_vert_advance},
  {"inset", ltext_get_inset},

  {"style", ltext_get_style},
  {"shadow_color", ltext_get_shadow_color},
  {"glow_color", ltext_get_glow_color},
  {"glow2_color", ltext_get_glow2_color},
  {"outline_color", ltext_get_outline_color},

  {"style_pad", ltext_get_style_pad},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lText_set[] = {
  {"font_source", ltext_set_font_source},
  {"font", ltext_set_font},
  {"text", ltext_set_text},
  {"style", ltext_set_style},
  {"shadow_color", ltext_set_shadow_color},
  {"glow_color", ltext_set_glow_color},
  {"glow2_color", ltext_set_glow2_color},
  {"outline_color", ltext_set_outline_color},
  {NULL, NULL}			// sentinel
};

const luaL_Reg lText_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mText = {
  lText_nil,
  lText_get,
  lText_set,
  lText_nil
};

const luaobj_Reg *cText[] = {
  &mClass,
  &mObject,
  &mText,
  NULL				// sentinel
};
