#include "class.h"
#include "transform.h"
#include <Evas.h>

static int
ltransform_get_matrix (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	Evas_Transform *trans = (Evas_Transform *) obj->data;
	lua_newtable (L);
		lua_newtable (L);
			lua_pushnumber (L, trans->mxx); lua_rawseti (L, -2, 1);
			lua_pushnumber (L, trans->mxy); lua_rawseti (L, -2, 2);
			lua_pushnumber (L, trans->mxz); lua_rawseti (L, -2, 3);
		lua_rawseti (L, -2, 1);
		lua_newtable (L);
			lua_pushnumber (L, trans->myx); lua_rawseti (L, -2, 1);
			lua_pushnumber (L, trans->myy); lua_rawseti (L, -2, 2);
			lua_pushnumber (L, trans->myz); lua_rawseti (L, -2, 3);
		lua_rawseti (L, -2, 2);
		lua_newtable (L);
			lua_pushnumber (L, trans->mzx); lua_rawseti (L, -2, 1);
			lua_pushnumber (L, trans->mzy); lua_rawseti (L, -2, 2);
			lua_pushnumber (L, trans->mzz); lua_rawseti (L, -2, 3);
		lua_rawseti (L, -2, 3);
	return 1;
}

static int
ltransform_set_matrix (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	Evas_Transform *trans = (Evas_Transform *) obj->data;
	lua_rawgeti (L, 2, 1);
		lua_rawgeti (L, -1, 1); trans->mxx = luaL_checknumber (L, -1);
		lua_rawgeti (L, -2, 2); trans->mxy = luaL_checknumber (L, -1);
		lua_rawgeti (L, -3, 3); trans->mxz = luaL_checknumber (L, -1);
	lua_pop (L, 4);
	lua_rawgeti (L, 2, 2);
		lua_rawgeti (L, -1, 1); trans->myx = luaL_checknumber (L, -1);
		lua_rawgeti (L, -2, 2); trans->myy = luaL_checknumber (L, -1);
		lua_rawgeti (L, -3, 3); trans->myz = luaL_checknumber (L, -1);
	lua_pop (L, 4);
	lua_rawgeti (L, 2, 2);
		lua_rawgeti (L, -1, 1); trans->mzx = luaL_checknumber (L, -1);
		lua_rawgeti (L, -2, 2); trans->mzy = luaL_checknumber (L, -1);
		lua_rawgeti (L, -3, 3); trans->mzz = luaL_checknumber (L, -1);
	lua_pop (L, 4);
	return 0;
}

/*
 * transform function methods
 */

static int
ltransform_fn_rotate (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	evas_transform_rotate (
		luaL_checknumber (L, 2),
		obj->data);
	return 0;
}

static int
ltransform_fn_translate (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	evas_transform_translate (
		luaL_checknumber (L, 2),
		luaL_checknumber (L, 3),
		obj->data);
	return 0;
}

static int
ltransform_fn_scale (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	evas_transform_scale (
		luaL_checknumber (L, 2),
		luaL_checknumber (L, 3),
		obj->data);
	return 0;
}

static int
ltransform_fn_shear (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	evas_transform_shear (
		luaL_checknumber (L, 2),
		luaL_checknumber (L, 3),
		obj->data);
	return 0;
}

static int
ltransform_fn_compose (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Object *lef = (luaobj_Object *) lua_touserdata (L, 2);
	evas_transform_compose (
		lef->data,
		obj->data);
	return 0;
}

static int
ltransform_fn_identity (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	evas_transform_identity_set (obj->data);
	return 0;
}

const luaL_Reg lTransform_get [] = {
	{"matrix", ltransform_get_matrix},
	{NULL, NULL} // sentinel
};

const luaL_Reg lTransform_set [] = {
	{"matrix", ltransform_set_matrix},
	{NULL, NULL} // sentinel
};

const luaL_Reg lTransform_fn [] = {
	{"rotate", ltransform_fn_rotate},
	{"translate", ltransform_fn_translate},
	{"scale", ltransform_fn_scale},
	{"shear", ltransform_fn_shear},
	{"compose", ltransform_fn_compose},
	{"identity", ltransform_fn_identity},
	{NULL, NULL} // sentinel
};

const luaL_Reg lTransform_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mTransform = {
  lTransform_nil,
  lTransform_get,
  lTransform_set,
  lTransform_fn
};

const luaobj_Reg *cTransform[] = {
  &mClass,
  &mTransform,
  NULL				// sentinel
};

