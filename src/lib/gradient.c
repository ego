#include "gradient.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"

static int
lgradient_fn_color_stop_add (lua_State * L)
{
  /* 1: gradient
   * 2: table
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);	// r
  lua_rawgeti (L, 2, 2);	// g
  lua_rawgeti (L, 2, 3);	// b
  lua_rawgeti (L, 2, 4);	// alpha
  lua_rawgeti (L, 2, 5);	// delta
  evas_object_gradient_color_stop_add (obj->data,
				       (int) luaL_checknumber (L, -5),
				       (int) luaL_checknumber (L, -4),
				       (int) luaL_checknumber (L, -3),
				       (int) luaL_checknumber (L, -2),
				       (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lgradient_fn_alpha_stop_add (lua_State * L)
{
  /* 1: gradient
   * 2: table
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);	// alpha
  lua_rawgeti (L, 2, 2);	// delta
  evas_object_gradient_alpha_stop_add (obj->data,
				       (int) luaL_checknumber (L, -2),
				       (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lgradient_fn_clear (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_clear (obj->data);
  return 0;
}

static int
lgradient_get_type (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  char *type, *type_params;
  evas_object_gradient_type_get (obj->data, &type, &type_params);
  lua_newtable (L);
  lua_pushstring (L, type);
  lua_rawseti (L, -2, 1);
  lua_pushstring (L, type_params);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
lgradient_get_fill (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int x, y, w, h;
  evas_object_gradient_fill_get (obj->data, &x, &y, &w, &h);
  lua_newtable (L);
  lua_pushnumber (L, x);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, y);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, w);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, h);
  lua_rawseti (L, -2, 4);;
  return 1;
}

static int
lgradient_get_fill_angle (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_gradient_fill_angle_get (obj->data));
  return 1;
}

static int
lgradient_get_fill_spread (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_gradient_fill_spread_get (obj->data));
  return 1;
}

static int
lgradient_get_angle (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_gradient_angle_get (obj->data));
  return 1;
}

static int
lgradient_get_direction (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_gradient_direction_get (obj->data));
  return 1;
}

static int
lgradient_get_offset (lua_State * L)
{
  /* 1: gradient
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_gradient_offset_get (obj->data));
  return 1;
}

static int
lgradient_set_type (lua_State * L)
{
  /* 1: gradient
   * 2: table
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  evas_object_gradient_type_set (obj->data,
				 luaL_checkstring (L, -2), luaL_checkstring (L, -1));
  return 0;
}

static int
lgradient_set_fill (lua_State * L)
{
  /* 1: gradient
   * 2: table
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  lua_rawgeti (L, 2, 3);
  lua_rawgeti (L, 2, 4);
  evas_object_gradient_fill_set (obj->data,
				 (int) luaL_checknumber (L, -4),
				 (int) luaL_checknumber (L, -3),
				 (int) luaL_checknumber (L, -2),
				 (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lgradient_set_fill_angle (lua_State * L)
{
  /* 1: gradient
   * 2: fill_angle
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_fill_angle_set (obj->data, (int) luaL_checknumber (L, 2));
  return 0;
}

static int
lgradient_set_fill_spread (lua_State * L)
{
  /* 1: gradient
   * 2: fill_spread
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_fill_spread_set (obj->data,
					(int) luaL_checknumber (L, 2));
  return 0;
}

static int
lgradient_set_angle (lua_State * L)
{
  /* 1: gradient
   * 2: angle
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_angle_set (obj->data, (int) luaL_checknumber (L, 2));
  return 0;
}

static int
lgradient_set_direction (lua_State * L)
{
  /* 1: gradient
   * 2: direction
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_direction_set (obj->data, (int) luaL_checknumber (L, 2));
  return 0;
}

static int
lgradient_set_offset (lua_State * L)
{
  /* 1: gradient
   * 2: offset
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_gradient_offset_set (obj->data, (float) luaL_checknumber (L, 1));
  return 0;
}

const struct luaL_Reg lGradient_fn[] = {
  {"color_stop_add", lgradient_fn_color_stop_add},
  {"alpha_stop_add", lgradient_fn_alpha_stop_add},
  {"clear", lgradient_fn_clear},

  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient_get[] = {
  {"type", lgradient_get_type},

  {"fill", lgradient_get_fill},
  {"fill_angle", lgradient_get_fill_angle},
  {"fill_spread", lgradient_get_fill_spread},

  {"angle", lgradient_get_angle},
  {"direction", lgradient_get_direction},
  {"offset", lgradient_get_offset},

  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient_set[] = {
  {"type", lgradient_set_type},

  {"fill", lgradient_set_fill},
  {"fill_angle", lgradient_set_fill_angle},
  {"fill_spread", lgradient_set_fill_spread},

  {"angle", lgradient_set_angle},
  {"direction", lgradient_set_direction},
  {"offset", lgradient_set_offset},

  {NULL, NULL}			// sentinel
};

const luaL_Reg lGradient_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mGradient = {
  lGradient_nil,		// mt
  lGradient_get,
  lGradient_set,
  lGradient_fn			// fn
};

const luaobj_Reg *cGradient[] = {
  &mClass,
  &mObject,
  &mGradient,
  NULL				// sentinel
};
