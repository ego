#include "class.h"
#include "evas.h"
#include "object.h"
#include "image.h"
#include "animator.h"
#include "smart.h"
#include "ego.h"

#include <Evas.h>
#include <Edje.h>

extern luaobj_Ref *rSmart;

/* FIXME
 * smart_member_add of edje and ego don't like each other
 */

#define FN_ADD_OBJ(EGO_FUNC, EGO_CLASS, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	int set = lua_gettop (L) == 2; \
	luaobj_Object *obj = luaL_checkudata (L, 1, "cEgo"); \
	luaobj_Object *tar = lua_newuserdata (L, sizeof (luaobj_Object)); \
	luaobj_set_class (L, -1, tar, EGO_CLASS); \
  tar->data = E_FUNC (evas_object_evas_get (obj->data)); \
  evas_object_data_set (tar->data, "luaobj", luaobj_new_ref (L, -1)); \
	evas_object_smart_member_add (tar->data, obj->data); \
	evas_object_clip_set (tar->data, evas_object_smart_clipped_clipper_get (obj->data)); \
	if (set) \
	{ \
		lua_getfield (L, -1, "set"); \
		lua_pushvalue (L, -2); \
		lua_pushvalue (L, 2); \
		if (lua_pcall (L, 2, 0, 0)) \
			printf (EGO_CLASS"#set: %s\n", luaL_checkstring (L, -1)); \
	} \
	return 1; \
}

#define FN_ADD_ELM(EGO_FUNC, EGO_CLASS, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	int set = lua_gettop (L) == 2; \
	luaobj_Object *obj = luaL_checkudata (L, 1, "cEgo"); \
	luaobj_Object *tar = lua_newuserdata (L, sizeof (luaobj_Object)); \
	luaobj_set_class (L, -1, tar, EGO_CLASS); \
  tar->data = (Evas_Object *) E_FUNC (obj->data); \
  evas_object_data_set (tar->data, "luaobj", luaobj_new_ref (L, -1)); \
	evas_object_smart_member_add (tar->data, obj->data); \
	evas_object_clip_set (tar->data, evas_object_smart_clipped_clipper_get (obj->data)); \
	if (set) \
	{ \
		lua_getfield (L, -1, "set"); \
		lua_pushvalue (L, -2); \
		lua_pushvalue (L, 2); \
		if (lua_pcall (L, 2, 0, 0)) \
			printf (EGO_CLASS"#set: %s\n", luaL_checkstring (L, -1)); \
	} \
	return 1; \
}

static int
lecore_fn_animator (lua_State * L)
{
  /* 1: ego
	 * 2: function
	 * 3: (table)
   */
  luaobj_Object *obj = lua_newuserdata (L, sizeof (luaobj_Object));
  luaobj_set_class (L, -1, obj, "cAnimator");
  obj->data = ecore_animator_add (lanimator_cb, obj);
  obj->datb = luaobj_new_ref (L, -1); // userdata
  obj->datc = luaobj_new_ref (L, 2); // function
  return 1;
}

static int
levas_fn_smart_class (lua_State * L)
{
  /* 1: evas (not used)
   * 2: table {}
   */
  luaobj_Object *sma =
    (luaobj_Object *) lua_newuserdata (L, sizeof (luaobj_Object));
  luaobj_set_class (L, -1, sma, "cSmart");
  sma->data = (void *) lsmart_class_new (luaobj_new_ref (L, 2));
  return 1;
}

static int
levas_fn_smart (lua_State * L)
{
  /* 1: evas
   * 2: smart_class
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Object *sma = (luaobj_Object *) lua_touserdata (L, 2);
  luaobj_Object *sob =
    (luaobj_Object *) lua_newuserdata (L, sizeof (luaobj_Object));
  luaobj_set_class (L, -1, sob, "cRect");
	rSmart = luaobj_new_ref (L, -1);
  sob->data = evas_object_smart_add (evas_object_evas_get (obj->data), sma->data);
  evas_object_data_set (sob->data, "luaobj", rSmart);
  return 1;
}

static int
levas_fn_textblock_style (lua_State * L)
{
  /* 1: evas (not used)
   */
	int set = lua_gettop (L) == 2;
  luaobj_Object *sty =
    (luaobj_Object *) lua_newuserdata (L, sizeof (luaobj_Object));
  luaobj_set_class (L, -1, sty, "cStyle");
  sty->data = evas_textblock_style_new ();
  // FIXME // register
	if (set)
	{
		lua_getfield (L, -1, "set");
		lua_pushvalue (L, -2);
		lua_pushvalue (L, 2);
		if (lua_pcall (L, 2, 0, 0))
			printf ("textblock_style#set: %s\n", luaL_checkstring (L, -1));
	}
  return 1;
}

static int
levas_fn_transform (lua_State * L)
{
  /* 1: evas (not used)
   */
	int set = lua_gettop (L) == 2;
  luaobj_Object *sty =
    (luaobj_Object *) lua_newuserdata (L, sizeof (luaobj_Object));
  luaobj_set_class (L, -1, sty, "cTransform");
  sty->data = malloc (sizeof (Evas_Transform));
  // FIXME // register
	if (set)
	{
		lua_getfield (L, -1, "set");
		lua_pushvalue (L, -2);
		lua_pushvalue (L, 2);
		if (lua_pcall (L, 2, 0, 0))
			printf ("transform#set: %s\n", luaL_checkstring (L, -1));
	}
  return 1;
}

FN_ADD_OBJ (levas_fn_rectangle, "cRect", evas_object_rectangle_add);
FN_ADD_OBJ (levas_fn_image, "cImage", evas_object_image_add);
FN_ADD_OBJ (levas_fn_edje, "cEdje", edje_object_add);
FN_ADD_OBJ (levas_fn_text, "cText", evas_object_text_add);
FN_ADD_OBJ (levas_fn_textblock, "cTextblock", evas_object_textblock_add);
FN_ADD_OBJ (levas_fn_gradient, "cGradient", evas_object_gradient_add);
FN_ADD_OBJ (levas_fn_gradient2_linear, "cGradient2_Linear", evas_object_gradient2_linear_add);
FN_ADD_OBJ (levas_fn_gradient2_radial, "cGradient2_Radial", evas_object_gradient2_radial_add);
FN_ADD_OBJ (levas_fn_polygon, "cPolygon", evas_object_polygon_add);
FN_ADD_OBJ (levas_fn_line, "cLine", evas_object_line_add);
FN_ADD_OBJ (levas_fn_table, "cTable", evas_object_table_add);
FN_ADD_OBJ (levas_fn_box, "cBox", evas_object_box_add);

FN_ADD_ELM (levas_fn_icon, "cIcon", elm_icon_add);
FN_ADD_ELM (levas_fn_box2, "cBox2", elm_box_add);
FN_ADD_ELM (levas_fn_button, "cButton", elm_button_add);
FN_ADD_ELM (levas_fn_scroller, "cScroller", elm_scroller_add);
FN_ADD_ELM (levas_fn_label, "cLabel", elm_label_add);
FN_ADD_ELM (levas_fn_toggle, "cToggle", elm_toggle_add);
FN_ADD_ELM (levas_fn_frame, "cFrame", elm_frame_add);
FN_ADD_ELM (levas_fn_table2, "cTable2", elm_table_add);
FN_ADD_ELM (levas_fn_entry, "cEntry", elm_entry_add);
FN_ADD_ELM (levas_fn_bubble, "cBubble", elm_bubble_add);
FN_ADD_ELM (levas_fn_hover, "cHover", elm_hover_add);
FN_ADD_ELM (levas_fn_layout, "cLayout", elm_layout_add);
FN_ADD_ELM (levas_fn_clock, "cClock", elm_clock_add);

static int
levas_fn_quit (lua_State * L)
{
	ecore_main_loop_quit ();
  return 0;
}

/*
 * evas functions definitions
 */

const luaL_Reg lEgo_fn [] = {
	{"animator", lecore_fn_animator},
  {"rectangle", levas_fn_rectangle},
  {"image", levas_fn_image},
	{"edje", levas_fn_edje},
  {"line", levas_fn_line},
  {"text", levas_fn_text},
  {"textblock", levas_fn_textblock},
  {"gradient", levas_fn_gradient},
  {"gradient2_linear", levas_fn_gradient2_linear},
  {"gradient2_radial", levas_fn_gradient2_radial},
  {"polygon", levas_fn_polygon},
  {"table", levas_fn_table},
  {"box", levas_fn_box},

  {"textblock_style", levas_fn_textblock_style},
  {"smart_class", levas_fn_smart_class},
  {"smart", levas_fn_smart},
	{"transform", levas_fn_transform},

  {"icon", levas_fn_icon},
  {"box2", levas_fn_box2},
  {"button", levas_fn_button},
	{"scroller", levas_fn_scroller},
	{"label", levas_fn_label},
	{"toggle", levas_fn_toggle},
	{"frame", levas_fn_frame},
	{"table2", levas_fn_table2},
	{"entry", levas_fn_entry},
	{"bubble", levas_fn_bubble},
	{"hover", levas_fn_hover},
	{"layout", levas_fn_layout},
	{"clock", levas_fn_clock},

	{"quit", levas_fn_quit},

	{NULL, NULL}
};

const luaL_Reg lEgo_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mEgo = {
	lEgo_nil,
	lEgo_nil,
	lEgo_nil,
	lEgo_fn
};

const luaobj_Reg *cEgo [] = {
	&mClass,
	&mObject,
	&mEgo,
	NULL // sentinel
};

