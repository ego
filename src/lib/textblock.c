#include "textblock.h"
#include "textblock_style.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"

GET_STRING (ltextblock_get_text_markup, evas_object_textblock_text_markup_get);

SET_OBJ (ltextblock_set_style, evas_object_textblock_style_set);
SET_STRING (ltextblock_set_text_markup, evas_object_textblock_text_markup_set);

const struct luaL_Reg lTextblock_get[] = {
  {"text_markup", ltextblock_get_text_markup},

  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lTextblock_set[] = {
  {"style", ltextblock_set_style},
  {"text_markup", ltextblock_set_text_markup},

  {NULL, NULL}			// sentinel
};

const luaL_Reg lTextblock_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mTextblock = {
  lTextblock_nil,
  lTextblock_get,
  lTextblock_set,
  lTextblock_nil
};

const luaobj_Reg *cTextblock[] = {
  &mClass,
  &mObject,
  &mTextblock,
  NULL				// sentinel
};

