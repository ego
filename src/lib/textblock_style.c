#include "textblock_style.h"
#include "luaobj.h"
#include "class.h"
#include "macro.h"

GET_STRING (ltextblock_style_get_style, evas_textblock_style_get);

const struct luaL_Reg lStyle_get [] = {
	{"style",ltextblock_style_get_style},
	{NULL,NULL} // sentinel
};

SET_STRING (ltextblock_style_set_style, evas_textblock_style_set);

const struct luaL_Reg lStyle_set [] = {
	{"style",ltextblock_style_set_style},
	{NULL,NULL} // sentinel
};

FN (ltextblock_style_fn_free, evas_textblock_style_free);

const struct luaL_Reg lStyle_fn [] = { 
	{"free",ltextblock_style_fn_free},
	{NULL,NULL} // sentinel
};

const struct luaL_Reg lStyle_mt [] = {
	{"__gc", ltextblock_style_fn_free},
	{NULL, NULL}
};

const luaobj_Reg mStyle = {
	lStyle_mt, // mt
	lStyle_get, // get
	lStyle_set, // set
	lStyle_fn
};

const luaobj_Reg* cStyle [] = {
	&mClass,
	&mStyle,
	NULL // sentinel
};

