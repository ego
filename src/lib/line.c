#include "line.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"

static int
lline_get_xy (lua_State * L)
{
  /* 1: line
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int x1, y1, x2, y2;
  evas_object_line_xy_get (obj->data, &x1, &y1, &x2, &y2);
  lua_newtable (L);
  lua_pushnumber (L, x1);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, y1);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, x2);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, y2);
  lua_rawseti (L, -3, 4);
  return 1;
}

static int
lline_set_xy (lua_State * L)
{
  /* 1: line
   * 2: table {x1,y2,x2,y2}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  lua_rawgeti (L, 2, 3);
  lua_rawgeti (L, 2, 4);
  evas_object_line_xy_set (obj->data,
			   (int) luaL_checknumber (L, -4),
			   (int) luaL_checknumber (L, -3),
			   (int) luaL_checknumber (L, -2),
			   (int) luaL_checknumber (L, -1));
  return 0;
}

const struct luaL_Reg lLine_get[] = {
  {"xy", lline_get_xy},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lLine_set[] = {
  {"xy", lline_set_xy},
  {NULL, NULL}			// sentinel
};
const luaL_Reg lLine_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mLine = {
  lLine_nil,			// mt
  lLine_get,
  lLine_set,
  lLine_nil			// fn
};

const luaobj_Reg *cLine [] = {
  &mClass,
  &mObject,
  &mLine,
  NULL				// sentinel
};
