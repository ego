#include "luaobj.h"
#include "class.h"
#include "animator.h"
#include "macro.h"

int
lanimator_cb (void *DATA)
{
  luaobj_Object *obj = (luaobj_Object *) DATA;
  luaobj_Ref *ref3 = (luaobj_Ref *) obj->datc; // function
  luaobj_Ref *ref2 = (luaobj_Ref *) obj->datb; // userdata
  lua_State *L = obj->L;
  luaobj_get_ref (L, ref3);
  luaobj_get_ref (L, ref2);

  if (lua_pcall (L, 1, 1, 0))
    printf ("lanimator_cb: %s\n", luaL_checkstring (L, -1));
  int res = (int) lua_toboolean (L, -1);
  if (res)
    return 1;
  else
    return 0;
}

FN (lanimator_fn_del, ecore_animator_del);

const luaL_Reg lAnimator_nil[] = {
	{NULL, NULL} // sentinel
};

const luaL_Reg lAnimator_fn[] = {
  {"del", lanimator_fn_del},
  {NULL, NULL}			// sentinel
};

const luaobj_Reg mAnimator = {
  lAnimator_nil,				// mt
  lAnimator_nil,				// get
  lAnimator_nil,				// set
  lAnimator_fn
};

const luaobj_Reg *cAnimator [] = {
  &mClass,
  &mAnimator,
  NULL				// sentinel
};

