#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "smart.h"

void
lsmart_cb (luaobj_Ref * ref1, Evas_Object * obj, const char *key)
{
  printf ("obj_cb: %s\n", key);
  lua_State *L = ref1->L;
  luaobj_get_ref (L, ref1);	// tab
  lua_getfield (L, -1, key);	// tab[key]
  lua_remove (L, -2);		// tab
  //luaobj_Ref *ref2 = (luaobj_Ref *) evas_object_data_get (obj, "luaobj");
	luaobj_Ref *ref2 = rSmart;
  luaobj_get_ref (L, ref2);	// obj
}

void
lsmart_add (Evas_Object * obj)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "add");
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#add: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_del (Evas_Object * obj)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "del");
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#del: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_move (Evas_Object * obj, Evas_Coord x, Evas_Coord y)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "move");
  lua_pushnumber (L, x);
  lua_pushnumber (L, y);
  if (lua_pcall (L, 3, 0, 0))
    printf ("lsmart#move: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_resize (Evas_Object * obj, Evas_Coord w, Evas_Coord h)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "resize");
  lua_pushnumber (L, w);
  lua_pushnumber (L, h);
  if (lua_pcall (L, 3, 0, 0))
    printf ("lsmart#resize: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_show (Evas_Object * obj)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "show");
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#show: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_hide (Evas_Object * obj)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "hide");
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#hide: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_color_set (Evas_Object * obj, int r, int g, int b, int a)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "color_set");
  lua_newtable (L);
  lua_pushnumber (L, r);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, g);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, b);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, a);
  lua_rawseti (L, -2, 4);
  if (lua_pcall (L, 2, 0, 0))
    printf ("lsmart#color_set: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_clip_set (Evas_Object * obj, Evas_Object * clip)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "clip_set");
  luaobj_Ref *ref2 = (luaobj_Ref *) evas_object_data_get (clip, "luaobj");
  luaobj_get_ref (L, ref2);
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#clip_set: %s\n", luaL_checkstring (L, -1));
}

void
lsmart_clip_unset (Evas_Object * obj)
{
  Evas_Smart *smart = evas_object_smart_smart_get (obj);
  luaobj_Ref *ref = (luaobj_Ref *) evas_smart_data_get (smart);
  lua_State *L = ref->L;
  lsmart_cb (ref, obj, "clip_unset");
  if (lua_pcall (L, 1, 0, 0))
    printf ("lsmart#clip_unset: %s\n", luaL_checkstring (L, -1));
}

const luaobj_Reg *cSmart[] = {
  &mClass,
  NULL				// sentinel
};

luaobj_Ref *rSmart = NULL;

Evas_Smart *
lsmart_class_new (void *data)
{
	//printf ("lsmart_class_new\n");
	Evas_Smart_Class *sc = (Evas_Smart_Class *) malloc (sizeof (Evas_Smart_Class));
	sc->name = "lsmart";
	sc->version = EVAS_SMART_CLASS_VERSION;
	sc->add = lsmart_add;
	sc->hide = lsmart_del;
	sc->move = lsmart_move;
	sc->resize = lsmart_resize;
	sc->show = lsmart_show;
	sc->del = lsmart_hide;
	sc->color_set = lsmart_color_set;
	sc->clip_set = lsmart_clip_set;
	sc->clip_unset = lsmart_clip_unset;
	sc->data = data;

  return evas_smart_class_new (sc);
}

