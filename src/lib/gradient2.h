#ifndef EGO_EVAS_GRADIENT2_H
#define EGO_EVAS_GRADIENT2_H

#include "luaobj.h"

extern const luaobj_Reg *cGradient2_Linear[];
extern const luaobj_Reg *cGradient2_Radial[];

#endif
