#ifndef EGO_H
#define EGO_H

#include <lua.h>
#include <Evas.h>

void luaopen_ego (lua_State *L);
void ego_sandbox (lua_State *L, int level);

Evas_Object *ego_object_add (Evas *evas, lua_State *L);

#endif /* EGO_H */

