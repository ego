#include "gradient2.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"
#include <Evas.h>

static int
lgradient2_fn_color_np_stop_insert (lua_State *L)
{
	luaobj_Object *obj = lua_touserdata (L, 1);
	lua_rawgeti (L, 2, 1); // r
	lua_rawgeti (L, 2, 2); // g
	lua_rawgeti (L, 2, 3); // b
	lua_rawgeti (L, 2, 4); // a
	lua_rawgeti (L, 2, 5); // pos
	evas_object_gradient2_color_np_stop_insert (
		obj->data,
		luaL_checkint (L, -5),
		luaL_checkint (L, -4),
		luaL_checkint (L, -3),
		luaL_checkint (L, -2),
		luaL_checknumber (L, -1));
	return 0;
}

static int
lgradient2_get_fill_transform (lua_State *L)
{
	luaobj_Object *obj = lua_touserdata (L, 1);
	Evas_Transform *trans = NULL;
	evas_object_gradient2_fill_transform_get (obj->data, trans);
	// FIXME registry
	lua_pushnil (L);
	return 1;
}
GET_INTEGER (lgradient2_get_fill_spread, evas_object_gradient2_fill_spread_get);
SET_INTEGER (lgradient2_set_fill_spread, evas_object_gradient2_fill_spread_set);
SET_OBJ (lgradient2_set_fill_transform, evas_object_gradient2_fill_transform_set);

GET_TAB4INTEGER (lgradient2_linear_get_fill, evas_object_gradient2_linear_fill_get);
GET_TAB4FLOAT (lgradient2_radial_get_fill, evas_object_gradient2_radial_fill_get);

SET_TAB4INTEGER (lgradient2_linear_set_fill, evas_object_gradient2_linear_fill_set);
SET_TAB4FLOAT (lgradient2_radial_set_fill, evas_object_gradient2_radial_fill_set);

const struct luaL_Reg lGradient2_fn[] = {
	{"color_np_stop_insert", lgradient2_fn_color_np_stop_insert},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_get[] = {
	{"fill_spread", lgradient2_get_fill_spread},
	{"fill_transform", lgradient2_get_fill_transform},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_set[] = {
	{"fill_spread", lgradient2_set_fill_spread},
	{"fill_transform", lgradient2_set_fill_transform},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_Linear_get[] = {
	{"fill", lgradient2_linear_get_fill},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_Linear_set[] = {
	{"fill", lgradient2_linear_set_fill},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_Radial_get[] = {
	{"fill", lgradient2_radial_get_fill},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lGradient2_Radial_set[] = {
	{"fill", lgradient2_radial_set_fill},
  {NULL, NULL}			// sentinel
};

const luaL_Reg lGradient2_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mGradient2 = {
  lGradient2_nil,		// mt
  lGradient2_get,
  lGradient2_set,
  lGradient2_fn
};

const luaobj_Reg mGradient2_Linear = {
  lGradient2_nil,		// mt
  lGradient2_Linear_get,
  lGradient2_Linear_set,
  lGradient2_nil
};

const luaobj_Reg mGradient2_Radial = {
  lGradient2_nil,		// mt
  lGradient2_Radial_get,
  lGradient2_Radial_set,
  lGradient2_nil
};

const luaobj_Reg *cGradient2_Linear[] = {
  &mClass,
  &mObject,
  &mGradient2,
  &mGradient2_Linear,
  NULL				// sentinel
};

const luaobj_Reg *cGradient2_Radial[] = {
  &mClass,
  &mObject,
  &mGradient2,
  &mGradient2_Radial,
  NULL				// sentinel
};

