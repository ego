#ifndef EGO_MACRO_H
#define EGO_MACRO_H

#include "luaobj.h"

/*
 * fn macros
 */

#define FN(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data); \
	return 1; \
}

/*
 * bool macros
 */

#define GET_BOOL(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_pushboolean (L, E_FUNC (obj->data)); \
	return 1; \
}

#define SET_BOOL(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, lua_toboolean (L, 2)); \
	return 0; \
}

/*
 * integer macros
 */

#define GET_INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_pushnumber (L, E_FUNC (obj->data)); \
	return 1; \
}

#define SET_INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkinteger (L, 2)); \
	return 0; \
}

/*
 * float macros
 */

#define GET_FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_pushnumber (L, E_FUNC (obj->data)); \
	return 1; \
}

#define SET_FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checknumber (L, 2)); \
	return 0; \
}

/*
 * string macros
 */

#define GET_STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_pushstring (L, (const char *) E_FUNC (obj->data)); \
	return 1; \
}

#define SET_STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkstring (L, 2)); \
	return 0; \
}

/*
 * obj macros
 */

#define GET_OBJ(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
  Evas_Object *tar = E_FUNC (obj->data); \
  if (tar != NULL) \
  { \
	  luaobj_Ref *ref = evas_object_data_get (tar, "luaobj"); \
    luaobj_get_ref (L, ref); \
  } \
  else \
    lua_pushnil (L); \
	return 1; \
}

#define SET_OBJ(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	luaobj_Object *tar = lua_touserdata (L, 2); \
	E_FUNC (obj->data, tar->data); \
	return 0; \
}

#define FN_BOOL(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, lua_toboolean (L, 2)); \
	return 0; \
}

#define FN_INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkint (L, 2)); \
	return 0; \
}

#define FN_2INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkint (L, 2), luaL_checkint (L, 3)); \
	return 0; \
}

#define FN_FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkinnumber (L, 2)); \
	return 0; \
}

#define FN_2FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkinnumber (L, 2), luaL_checknumber (L, 3)); \
	return 0; \
}

#define FN_STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC (obj->data, luaL_checkstring (L, 2)); \
	return 0; \
}

#define FN_2STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	E_FUNC ( \
		obj->data, \
		luaL_checkstring (L, 2), \
		luaL_checkstring (L, 3)); \
	return 0; \
}

#define FN_OBJ(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	luaobj_Object *tar = lua_touserdata (L, 2); \
	E_FUNC (obj->data, tar->data); \
	return 0; \
}

#define FN_2OBJ(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	luaobj_Object *tar1 = lua_touserdata (L, 2); \
	luaobj_Object *tar2 = lua_touserdata (L, 3); \
	E_FUNC (obj->data, tar1->data, tar2->data); \
	return 0; \
}

/*
 *  tab2bool macros
 */

#define GET_TAB2BOOL(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	int p1, p2; \
	E_FUNC (obj->data, &p1, &p2); \
	lua_newtable (L); \
	lua_pushboolean (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushboolean (L, p2); lua_rawseti (L, -2, 2); \
	return 1; \
}

#define SET_TAB2BOOL(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	E_FUNC ( \
		obj->data, \
		lua_toboolean (L, -2),\
		lua_toboolean (L, -1)); \
	return 0; \
}

/*
 *  tab2integer macros
 */

#define GET_TAB2INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	int p1, p2; \
	E_FUNC (obj->data, &p1, &p2); \
	lua_newtable (L); \
	lua_pushnumber (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushnumber (L, p2); lua_rawseti (L, -2, 2); \
	return 1; \
}

#define SET_TAB2INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	E_FUNC ( \
		obj->data, \
		luaL_checkinteger (L, -2),\
		luaL_checkinteger (L, -1)); \
	return 0; \
}

/*
 *  tab2float macros
 */

#define GET_TAB2FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	double p1, p2; \
	E_FUNC (obj->data, &p1, &p2); \
	lua_newtable (L); \
	lua_pushnumber (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushnumber (L, p2); lua_rawseti (L, -2, 2); \
	return 1; \
}

#define SET_TAB2FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	E_FUNC ( \
		obj->data, \
		luaL_checknumber (L, -2),\
		luaL_checknumber (L, -1)); \
	return 0; \
}

/*
 *  tab2string macros
 */

#define GET_TAB2STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	double p1, p2; \
	E_FUNC (obj->data, &p1, &p2); \
	lua_newtable (L); \
	lua_pushstring (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushstring (L, p2); lua_rawseti (L, -2, 2); \
	return 1; \
}

#define SET_TAB2STRING(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	E_FUNC ( \
		obj->data, \
		luaL_checkstring (L, -2),\
		luaL_checkstring (L, -1)); \
	return 0; \
}

/*
 *  tab3integer macros
 */

#define GET_TAB3INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	int p1, p2, p3; \
	E_FUNC (obj->data, &p1, &p2, &p3); \
	lua_newtable (L); \
	lua_pushnumber (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushnumber (L, p2); lua_rawseti (L, -2, 2); \
	lua_pushnumber (L, p3); lua_rawseti (L, -2, 3); \
	return 1; \
}

#define SET_TAB3INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	lua_rawgeti (L, 2, 3); \
	E_FUNC ( \
		obj->data, \
		luaL_checkinteger (L, -3),\
		luaL_checkinteger (L, -2),\
		luaL_checkinteger (L, -1)); \
	return 0; \
}

/*
 *  tab4integer macros
 */

#define GET_TAB4INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	int p1, p2, p3, p4; \
	E_FUNC (obj->data, &p1, &p2, &p3, &p4); \
	lua_newtable (L); \
	lua_pushnumber (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushnumber (L, p2); lua_rawseti (L, -2, 2); \
	lua_pushnumber (L, p3); lua_rawseti (L, -2, 3); \
	lua_pushnumber (L, p4); lua_rawseti (L, -2, 4); \
	return 1; \
}

#define SET_TAB4INTEGER(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	lua_rawgeti (L, 2, 3); \
	lua_rawgeti (L, 2, 4); \
	E_FUNC ( \
		obj->data, \
		luaL_checkinteger (L, -4),\
		luaL_checkinteger (L, -3),\
		luaL_checkinteger (L, -2),\
		luaL_checkinteger (L, -1)); \
	return 0; \
}

/*
 *  tab4float macros
 */

#define GET_TAB4FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	double p1, p2, p3, p4; \
	E_FUNC (obj->data, &p1, &p2, &p3, &p4); \
	lua_newtable (L); \
	lua_pushnumber (L, p1); lua_rawseti (L, -2, 1); \
	lua_pushnumber (L, p2); lua_rawseti (L, -2, 2); \
	lua_pushnumber (L, p3); lua_rawseti (L, -2, 3); \
	lua_pushnumber (L, p4); lua_rawseti (L, -2, 4); \
	return 1; \
}

#define SET_TAB4FLOAT(EGO_FUNC, E_FUNC) \
static int \
EGO_FUNC (lua_State *L) \
{ \
	luaobj_Object *obj = lua_touserdata (L, 1); \
	lua_rawgeti (L, 2, 1); \
	lua_rawgeti (L, 2, 2); \
	lua_rawgeti (L, 2, 3); \
	lua_rawgeti (L, 2, 4); \
	E_FUNC ( \
		obj->data, \
		luaL_checknumber (L, -4),\
		luaL_checknumber (L, -3),\
		luaL_checknumber (L, -2),\
		luaL_checknumber (L, -1)); \
	return 0; \
}

#endif

