#include "box.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"
#include <Evas.h>

GET_TAB2INTEGER (lbox_get_padding, evas_object_box_padding_get);
SET_TAB2INTEGER (lbox_set_padding, evas_object_box_padding_set);

GET_TAB2FLOAT (lbox_get_align, evas_object_box_align_get);
SET_TAB2FLOAT (lbox_set_align, evas_object_box_align_set);

FN_OBJ (lbox_fn_append, evas_object_box_append);
FN_OBJ (lbox_fn_prepend, evas_object_box_prepend);
FN_2OBJ (lbox_fn_insert_before, evas_object_box_insert_before);

static int
lbox_fn_insert_at (lua_State *L)
{
	luaobj_Object *obj = lua_touserdata (L, 1);
	luaobj_Object *tar = lua_touserdata (L, 2);
	evas_object_box_insert_at (
		obj->data,
		tar->data,
		luaL_checkinteger (L, 3));
	return 0;
}

FN_OBJ (lbox_fn_remove, evas_object_box_remove);
FN_INTEGER (lbox_fn_remove_at, evas_object_box_remove_at);

static int
lbox_set_layout (lua_State *L)
{
	luaobj_Object *obj = lua_touserdata (L, 1);
	const char *layout = luaL_checkstring (L, 2);
	if (!strcmp (layout, "horizontal"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_horizontal,
			NULL, NULL);
	else if (!strcmp (layout, "vertical"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_vertical,
			NULL, NULL);
	else if (!strcmp (layout, "homogeneous_horizontal"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_homogeneous_horizontal,
			NULL, NULL);
	else if (!strcmp (layout, "homogeneous_vertical"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_homogeneous_vertical,
			NULL, NULL);
	else if (!strcmp (layout, "homogeneous_max_size_horizontal"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_homogeneous_max_size_horizontal,
			NULL, NULL);
	else if (!strcmp (layout, "homogeneous_max_size_vertical"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_homogeneous_max_size_vertical,
			NULL, NULL);
	else if (!strcmp (layout, "flow_horizontal"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_flow_horizontal,
			NULL, NULL);
	else if (!strcmp (layout, "flow_vertical"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_flow_vertical,
			NULL, NULL);
	else if (!strcmp (layout, "stack"))
		evas_object_box_layout_set (
			obj->data,
			evas_object_box_layout_stack,
			NULL, NULL);
	else
		printf ("layout <%s> not found\n", layout);
	return 0;
}

const struct luaL_Reg lBox_fn[] = {
	{"append", lbox_fn_append},
	{"prepend", lbox_fn_prepend},
	{"insert_before", lbox_fn_insert_before},
	{"insert_at", lbox_fn_insert_at},
	{"remove", lbox_fn_remove},
	{"remove_at", lbox_fn_remove_at},
	{NULL, NULL} // sentinel
};

const struct luaL_Reg lBox_get[] = {
	{"padding", lbox_get_padding},
	{"align", lbox_get_align},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lBox_set[] = {
	{"layout", lbox_set_layout},
	{"padding", lbox_set_padding},
	{"align", lbox_set_align},
  {NULL, NULL}			// sentinel
};
const luaL_Reg lBox_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mBox = {
  lBox_nil,			// mt
  lBox_get,
  lBox_set,
  lBox_fn
};

const luaobj_Reg *cBox [] = {
  &mClass,
  &mObject,
  &mBox,
  NULL				// sentinel
};
