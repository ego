#include "polygon.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"

FN_2INTEGER (lpolygon_fn_point_add, evas_object_polygon_point_add);
FN (lpolygon_fn_points_clear, evas_object_polygon_points_clear);

const struct luaL_Reg lPolygon_fn[] = {
  {"point_add", lpolygon_fn_point_add},
  {"points_clear", lpolygon_fn_points_clear},
  {NULL, NULL}			// sentinel
};

const luaL_Reg lPolygon_nil [] = {
	{NULL, NULL}
};

const luaobj_Reg mPolygon = {
  lPolygon_nil,			// mt
  lPolygon_nil,			// get
  lPolygon_nil,			// set
  lPolygon_fn			// fn
};

const luaobj_Reg *cPolygon[] = {
  &mClass,
  &mObject,
  &mPolygon,
  NULL				// sentinel
};
