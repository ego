#include "table.h"
#include "luaobj.h"
#include "class.h"
#include "object.h"
#include "macro.h"

GET_INTEGER (ltable_set_homogeneous, evas_object_table_homogeneous_set);
SET_INTEGER (ltable_get_homogeneous, evas_object_table_homogeneous_get);

GET_TAB2INTEGER (ltable_get_padding, evas_object_table_padding_get);
SET_TAB2INTEGER (ltable_set_padding, evas_object_table_padding_set);

GET_TAB2FLOAT (ltable_get_align, evas_object_table_align_get);
SET_TAB2FLOAT (ltable_set_align, evas_object_table_align_set);

static int
ltable_fn_pack (lua_State *L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  luaobj_Object *pac = (luaobj_Object *) lua_touserdata (L, 2);
	evas_object_table_pack (
		obj->data, pac->data,
		luaL_checkint (L, 3), luaL_checkint (L, 4), // x, y
		luaL_checkint (L, 5), luaL_checkint (L, 6)); // w, h
	return 0;
}
FN_OBJ (ltable_fn_unpack, evas_object_table_unpack);
FN_BOOL (ltable_fn_clear, evas_object_table_clear);

GET_TAB2INTEGER (ltable_get_col_row_size, evas_object_table_col_row_size_get);

const struct luaL_Reg lTable_fn[] = {
	{"pack", ltable_fn_pack},
	{"unpack", ltable_fn_unpack},
	{"clear", ltable_fn_clear},
	{NULL, NULL} // sentinel
};

const struct luaL_Reg lTable_get[] = {
	{"homogeneous", ltable_get_homogeneous},
	{"padding", ltable_get_padding},
	{"align", ltable_get_align},
	{"col_row_size", ltable_get_col_row_size},
  {NULL, NULL}			// sentinel
};

const struct luaL_Reg lTable_set[] = {
	{"homogeneous", ltable_set_homogeneous},
	{"padding", ltable_set_padding},
	{"align", ltable_set_align},
  {NULL, NULL}			// sentinel
};
const luaL_Reg lTable_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mTable = {
  lTable_nil,			// mt
  lTable_get,
  lTable_set,
  lTable_fn
};

const luaobj_Reg *cTable [] = {
  &mClass,
  &mObject,
  &mTable,
  NULL				// sentinel
};
