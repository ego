#include "luaobj.h"

//--enum----------------------------------------------------------------//
void
luaobj_new_enum (lua_State * L, int index, const char *name,
	       const luaobj_Enum *enu)
{
  int n = 0;
  lua_newtable (L);
  while (enu[n].key != NULL)
	{
		lua_pushnumber (L, enu[n].val);
		lua_setfield (L, -2, enu[n].key);
		n += 1;
	}
  if (index < 0)
    lua_setfield (L, index - 1, name);
  else
    lua_setfield (L, index, name);
}

//--ref-----------------------------------------------------------------//

luaobj_Ref *
luaobj_new_ref (lua_State * L, int index)
{
  lua_pushvalue (L, index);
  luaobj_Ref *ref = (luaobj_Ref *) malloc (sizeof (luaobj_Ref *));
  ref->id = luaL_ref (L, LUA_REGISTRYINDEX);
  ref->L = L;
//printf("\tnew ref: %d\n",ref->id);
  return ref;
}

void
luaobj_get_ref (lua_State * L, luaobj_Ref * ref)
{
//printf("\tget ref %d\n",ref->id);
  lua_rawgeti (L, LUA_REGISTRYINDEX, ref->id);
}

void
luaobj_free_ref (lua_State * L, luaobj_Ref * ref)
{
//printf("\tfree ref: %d\n",ref->id);
  luaL_unref (L, LUA_REGISTRYINDEX, ref->id);
  free (ref);
}

//--rawset--------------------------------------------------------------//

void
luaobj_rawsetfield (lua_State * L, int index, const char *key)
{
  lua_pushstring (L, key);
  lua_insert (L, -2);
  if (index < 0)
    lua_rawset (L, index - 1);
  else
    lua_rawset (L, index);
}

void
luaobj_rawgetfield (lua_State * L, int index, const char *key)
{
  lua_pushstring (L, key);
  if (index < 0)
    lua_rawget (L, index - 1);
  else
    lua_rawget (L, index);
}

//--class---------------------------------------------------------------//

void
luaobj_new_class (lua_State * L, char *id, const luaobj_Reg **class)
{
  int n = 0;
  luaL_newmetatable (L, id);
  while (class[n] != NULL)
	{
		luaL_register (L, NULL, class[n]->mt);

		if (n == 0)
		{
			lua_newtable (L);
			luaL_register (L, NULL, class[n]->set);
			luaobj_rawsetfield (L, -2, ".set");

			lua_newtable (L);
			luaL_register (L, NULL, class[n]->get);
			luaobj_rawsetfield (L, -2, ".get");

			lua_newtable (L);
			luaL_register (L, NULL, class[n]->fn);
			luaobj_rawsetfield (L, -2, ".fn");
		}
    else
		{
			luaobj_rawgetfield (L, -1, ".set");
			luaL_register (L, NULL, class[n]->set);
			lua_pop (L, 1);

			luaobj_rawgetfield (L, -1, ".get");
			luaL_register (L, NULL, class[n]->get);
			lua_pop (L, 1);

			luaobj_rawgetfield (L, -1, ".fn");
			luaL_register (L, NULL, class[n]->fn);
			lua_pop (L, 1);
		}

    n += 1;
  }
  lua_pop (L, 1);		// class
}

void
luaobj_set_class (lua_State * L, int index, luaobj_Object *obj, const char *id)
{
  // create ref tab
  lua_pushvalue (L, -1);
  lua_newtable (L);
  lua_settable (L, LUA_REGISTRYINDEX);

  luaL_getmetatable (L, id);	// mt
  if (index < 0)
    lua_setmetatable (L, index - 1);
  else
    lua_setmetatable (L, index);

	obj->L = L;
	obj->type = id;
}

//--look----------------------------------------------------------------//

int
luaobj_look_fn (lua_State * L, const char *id, const char *key)
{
  luaL_getmetatable (L, id);	// mt
  luaobj_rawgetfield (L, -1, ".fn");
  lua_remove (L, -2);		// mt
  luaobj_rawgetfield (L, -1, key);	// mt[key]
  lua_remove (L, -2);		// .fn
  if (lua_iscfunction (L, -1))
    return 1;
  else
    {
      lua_pop (L, 1);		// nil
      return 0;
    }
}

int
luaobj_look_get (lua_State * L, const char *id, const char *key)
{
  luaL_getmetatable (L, id);	// mt
  luaobj_rawgetfield (L, -1, ".get");
  lua_remove (L, -2);		// mt
  luaobj_rawgetfield (L, -1, key);	// .get[key]
  lua_remove (L, -2);		// .get
  if (lua_iscfunction (L, -1))
    {
      lua_pushvalue (L, 1);
      if (lua_pcall (L, 1, 1, 0))
	printf ("%s#__index: .get: %s\n", id, luaL_checkstring (L, -1));
      return 1;
    }
  else
    {
      lua_pop (L, 1);		// nil
      return 0;
    }
}

int
luaobj_look_set (lua_State * L, const char *id)
{
  luaL_getmetatable (L, id);	// class
  luaobj_rawgetfield (L, -1, ".set");
  lua_remove (L, -2);		// class

  lua_pushvalue (L, 2);		// key
  lua_rawget (L, -2);		// .set[key]
  if (lua_iscfunction (L, -1))
    {
      lua_remove (L, -2);	// key
      lua_pushvalue (L, 1);	// obj
      lua_pushvalue (L, 3);	// value
      if (lua_pcall (L, 2, 0, 0))	// .set[key](obj,key,value)
	printf ("%s#__newindex: .set: %s\n", id, luaL_checkstring (L, -1));
      return 1;
    }
  else
    {
      lua_pop (L, 2);		// .set[key] // .set
      return 0;
    }
}

