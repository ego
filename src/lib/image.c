#include "class.h"
#include "object.h"
#include "image.h"
#include "macro.h"
#include <Evas.h>

/*
 * image function methods
 */

// convenience function, which fits image to its size
static int
limage_fn_fit (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  // get geometry
  Evas_Coord w, h;
  evas_object_geometry_get (obj->data, NULL, NULL, &w, &h);
  // resize
  evas_object_image_fill_set (obj->data, 0, 0, w, h);
  return 0;
}

static int
limage_fn_reload (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_reload (obj->data);
  return 0;
}

static int
limage_fn_save (lua_State * L)
{
  /* 1: image
   * 2: table {file,key,flags}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  lua_rawgeti (L, 2, 3);
  evas_object_image_save (obj->data,
			  luaL_checkstring (L, -3),
			  luaL_checkstring (L, -2), luaL_checkstring (L, -1));
  return 0;
}

/*
 * image get methods
 */

static int
limage_get_file (lua_State * L)
{
  /* 1: image
   */
  const char *file, *key;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_file_get (obj->data, &file, &key);

  lua_pushstring (L, key);
  return 1;
}

static int
limage_get_border (lua_State * L)
{
  /* 1: image
   */
  int l, r, t, b;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_border_get (obj->data, &l, &r, &t, &b);

  lua_newtable (L);
  lua_pushnumber (L, l);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, r);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, t);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, b);
  lua_rawseti (L, -2, 4);
  return 1;
}

static int
limage_get_border_center_fill (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushboolean (L,
		   (int) evas_object_image_border_center_fill_get (obj->
								   data));
  return 1;
}

static int
limage_get_fill (lua_State * L)
{
  /* 1: image
   */
  int x, y, w, h;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_fill_get (obj->data, &x, &y, &w, &h);
  lua_newtable (L);
  lua_pushnumber (L, x);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, y);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, w);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, h);
  lua_rawseti (L, -2, 4);
  return 1;
}

static int
limage_get_fill_spread (lua_State * L)
{
  /* 1: image
   */
  int x, y, w, h;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L,
		evas_object_image_fill_spread_get (obj->data));
  return 1;
}

static int
limage_get_size (lua_State * L)
{
  /* 1: image
   */
  int w, h;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_size_get (obj->data, &w, &h);
  lua_newtable (L);
  lua_pushnumber (L, w);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, h);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
limage_get_stride (lua_State * L)
{
  /* 1: image
   */
  int w, h;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L,
		evas_object_image_stride_get (obj->data));
  return 1;
}

static int
limage_get_load_error (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, evas_object_image_load_error_get (obj->data));
  return 1;
}

static int
limage_get_alpha (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushboolean (L, (int) evas_object_image_alpha_get (obj->data));
  return 1;
}

static int
limage_get_smooth_scale (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushboolean (L, (int) evas_object_image_smooth_scale_get (obj->data));
  return 1;
}

static int
limage_get_pixels_dirty (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushboolean (L, (int) evas_object_image_pixels_dirty_get (obj->data));
  return 1;
}

static int
limage_get_load_dpi (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L, (int) evas_object_image_load_dpi_get (obj->data));
  return 1;
}

static int
limage_get_load_size (lua_State * L)
{
  /* 1: image
   */
  int w, h;
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_load_size_get (obj->data, &w, &h);
  lua_newtable (L);
  lua_pushnumber (L, w);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, h);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
limage_get_load_scale_down (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushnumber (L,
		  (int) evas_object_image_load_scale_down_get (obj->data));
  return 1;
}

static int
limage_get_colorspace (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_pushboolean (L, (int) evas_object_image_colorspace_get (obj->data));
  return 1;
}

static int
limage_get_data (lua_State * L)
{
  /* 1: image
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	int x, y, w, h;
	int *pixels = evas_object_image_data_get (obj->data, 0);
	evas_object_image_size_get (obj->data, &w, &h);
	lua_newtable (L);
	for (x = 0; x < w; x++)
	{
		lua_newtable (L);
		for (y = 0; y < h; y++)
		{
			lua_pushnumber (L, pixels[x*w + y]);
			lua_rawseti (L, -2, y + 1);
		}
		lua_rawseti (L, -2, x + 1);
	}
  return 1;
}

static int
limage_get_fill_transform (lua_State * L)
{
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	Evas_Transform *trans = NULL;
	evas_object_image_fill_transform_get (obj->data, trans);
	// FIXME registry
	lua_pushnil (L);
	return 1;
}

/*
 * image set methods
 */

static int
limage_set_file (lua_State * L)
{
  /* 1: image
   * 2: image id
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  int key = luaL_checknumber (L, 2);
	char buf [256];
	sprintf (buf, "images/%i", key);
	lua_getfield (L, LUA_REGISTRYINDEX, "ego_file");
	char *file = luaL_checkstring (L, -1);
  evas_object_image_file_set (obj->data, file, buf);
  return 0;
}

static int
limage_set_border (lua_State * L)
{
  /* 1: image
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  lua_rawgeti (L, 2, 3);
  lua_rawgeti (L, 2, 4);
  evas_object_image_border_set (obj->data,
				(int) luaL_checknumber (L, -4),
				(int) luaL_checknumber (L, -3),
				(int) luaL_checknumber (L, -2),
				(int) luaL_checknumber (L, -1));
  return 0;
}

static int
limage_set_border_center_fill (lua_State * L)
{
  /* 1: image
   * 2: border_center_fill
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_border_center_fill_set (obj->data,
					    (int) lua_toboolean (L, 2));
  return 0;
}

static int
limage_set_fill (lua_State * L)
{
  /* 1: image
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  lua_rawgeti (L, 2, 3);
  lua_rawgeti (L, 2, 4);
  evas_object_image_fill_set (obj->data,
			      (int) luaL_checknumber (L, -4),
			      (int) luaL_checknumber (L, -3),
			      (int) luaL_checknumber (L, -2),
			      (int) luaL_checknumber (L, -1));
  return 0;
}

static int
limage_set_fill_spread (lua_State * L)
{
  /* 1: image
   * 2: int tile_mode
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_fill_spread_set (obj->data,
			      (int) luaL_checknumber (L, 2));
  return 0;
}

static int
limage_set_size (lua_State * L)
{
  /* 1: image
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  evas_object_image_size_set (obj->data,
			      (int) luaL_checknumber (L, -2),
			      (int) luaL_checknumber (L, -1));
  return 0;
}

static int
limage_set_alpha (lua_State * L)
{
  /* 1: image
   * 2: alpha
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_alpha_set (obj->data, (int) lua_toboolean (L, 2));
  return 0;
}

static int
limage_set_smooth_scale (lua_State * L)
{
  /* 1: image
   * 2: smooth_scale
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_smooth_scale_set (obj->data, (int) lua_toboolean (L, 2));
  return 0;
}

static int
limage_set_pixels_dirty (lua_State * L)
{
  /* 1: image
   * 2: pixels_dirty
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_pixels_dirty_set (obj->data, (int) lua_toboolean (L, 2));
  return 0;
}

static int
limage_set_load_dpi (lua_State * L)
{
  /* 1: image
   * 2: load_dpi
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_load_dpi_set (obj->data, luaL_checknumber (L, 2));
  return 0;
}

static int
limage_set_load_size (lua_State * L)
{
  /* 1: image
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  evas_object_image_load_size_set (obj->data,
				   (int) luaL_checknumber (L, -2),
				   (int) luaL_checknumber (L, -1));
  return 0;
}

static int
limage_set_load_scale_down (lua_State * L)
{
  /* 1: image
   * 2: load_scale_sown
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_load_scale_down_set (obj->data,
		luaL_checkint (L, 2));
  return 0;
}

static int
limage_set_colorspace (lua_State * L)
{
  /* 1: image
   * 2: colorspace
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  evas_object_image_colorspace_set (obj->data, (int) lua_toboolean (L, 2));
  return 0;
}

static int
limage_set_data (lua_State * L)
{
  /* 1: image
   * 2: tab
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
	int *data;
	// FIXME
  evas_object_image_data_copy_set (obj->data, data);
  return 0;
}

SET_OBJ (limage_set_fill_transform, evas_object_image_fill_transform_set);

/*
 * image function definitions
 */

const luaL_Reg lImage_fn [] = {
	{"fit", limage_fn_fit},
	{"reload", limage_fn_reload},
	{"save", limage_fn_save},
	{NULL, NULL} // sentinel
};

/*
 * image get definitions
 */

const struct luaL_Reg lImage_get [] = {
  {"file", limage_get_file},
  {"border", limage_get_border},
  {"border_center_fill", limage_get_border_center_fill},
  {"fill", limage_get_fill},
  {"fill_spread", limage_get_fill_spread},
  {"size", limage_get_size},
  {"stride", limage_get_stride},
  {"load_error", limage_get_load_error},
  {"alpha", limage_get_alpha},
  {"smooth_scale", limage_get_smooth_scale},
  {"pixels_dirty", limage_get_pixels_dirty},
  {"load_dpi", limage_get_load_dpi},
  {"load_size", limage_get_load_size},
  {"load_scale_down", limage_get_load_scale_down},
  {"colorspace", limage_get_colorspace},
	{"data", limage_get_data},
	{"fill_transform", limage_get_fill_transform},
  {NULL, NULL}			// sentinel
};

/*
 * image set definitions
 */

const struct luaL_Reg lImage_set[] = {
  {"file", limage_set_file},
  {"border", limage_set_border},
  {"border_center_fill", limage_set_border_center_fill},
  {"fill", limage_set_fill},
  {"fill_spread", limage_set_fill_spread},
  {"fill_transform", limage_set_fill_transform},
  {"size", limage_set_size},
  {"alpha", limage_set_alpha},
  {"smooth_scale", limage_set_smooth_scale},
  {"pixels_dirty", limage_set_pixels_dirty},
  {"load_dpi", limage_set_load_dpi},
  {"load_size", limage_set_load_size},
  {"load_scale_down", limage_set_load_scale_down},
  {"colorspace", limage_set_colorspace},
	{"data", limage_set_data},
	{"data_copy", limage_set_data},
  {NULL, NULL}			// sentinel
};

/*
 * image definitions
 */

const luaL_Reg lImage_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mImage = {
  lImage_nil,
  lImage_get,
  lImage_set,
  lImage_fn
};

const luaobj_Reg *cImage[] = {
  &mClass,
  &mObject,
  &mImage,
  NULL				// sentinel
};

