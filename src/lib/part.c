#include "part.h"
#include "class.h"
#include "object.h"

/*
 * edje part functions implementations
 */

static int
lpart_fn_unswallow (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  Evas_Object *swa = edje_object_part_swallow_get (edj, key);
  edje_object_part_unswallow (edj, swa);
  return 0;
}

/*
 * edje part get implementations
 */

static int
lpart_get_geometry (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  int x, y, w, h;
  edje_object_part_geometry_get (edj, key, &x, &y, &w, &h);
  lua_newtable (L);
  lua_pushnumber (L, x);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, y);
  lua_rawseti (L, -2, 2);
  lua_pushnumber (L, w);
  lua_rawseti (L, -2, 3);
  lua_pushnumber (L, h);
  lua_rawseti (L, -2, 4);
  return 1;
}

static int
lpart_get_text (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_pushstring (L, edje_object_part_text_get (edj, key));
  return 1;
}

static int
lpart_get_swallow (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  Evas_Object *swa = edje_object_part_swallow_get (edj, key);
  if (swa)
    {
      luaobj_Ref *ref = (luaobj_Ref *) evas_object_data_get (swa, "luaobj");
      luaobj_get_ref (L, ref);
    }
  else
    lua_pushnil (L);
  return 1;
}

static int
lpart_get_state (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  double val;
  const char *state = edje_object_part_state_get (edj, key, &val);
  lua_newtable (L);
  lua_pushstring (L, state);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, val);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
lpart_get_drag_dir (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_pushnumber (L, edje_object_part_drag_dir_get (edj, key));
  return 1;
}

static int
lpart_get_drag_value (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  double dx, dy;
  edje_object_part_drag_value_get (edj, key, &dx, &dy);
  lua_newtable (L);
  lua_pushnumber (L, dx);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, dy);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
lpart_get_drag_size (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  double dx, dy;
  edje_object_part_drag_size_get (edj, key, &dx, &dy);
  lua_newtable (L);
  lua_pushnumber (L, dx);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, dy);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
lpart_get_drag_step (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  double dx, dy;
  edje_object_part_drag_step_get (edj, key, &dx, &dy);
  lua_newtable (L);
  lua_pushnumber (L, dx);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, dy);
  lua_rawseti (L, -2, 2);
  return 1;
}

static int
lpart_get_drag_page (lua_State * L)
{
  /* 1: part
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  double dx, dy;
  edje_object_part_drag_page_get (edj, key, &dx, &dy);
  lua_newtable (L);
  lua_pushnumber (L, dx);
  lua_rawseti (L, -2, 1);
  lua_pushnumber (L, dy);
  lua_rawseti (L, -2, 2);
  return 1;
}

/*
 * edje part set implementtions
 */

static int
lpart_set_swallow (lua_State * L)
{
  /* 1: part
   * 2: swallow
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  luaobj_Object *swa = (luaobj_Object *) lua_touserdata (L, 2);
  edje_object_part_swallow (edj, key, swa->data);
	// FIXME set clip for elementary objects
  return 0;
}

static int
lpart_set_text (lua_State * L)
{
  /* 1: part
   * 2: text
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  edje_object_part_text_set (edj, key, luaL_checkstring (L, 2));
  return 0;
}

static int
lpart_set_drag_value (lua_State * L)
{
  /* 1: part
   * 2: table {dx,dy}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  edje_object_part_drag_value_set (edj, key,
				   (int) luaL_checknumber (L, -2),
				   (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lpart_set_drag_size (lua_State * L)
{
  /* 1: part
   * 2: table {dx,dy}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  edje_object_part_drag_size_set (edj, key,
				  (int) luaL_checknumber (L, -2),
				  (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lpart_set_drag_step (lua_State * L)
{
  /* 1: part
   * 2: table {dx,dy}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  edje_object_part_drag_step_set (edj, key,
				  (int) luaL_checknumber (L, -2),
				  (int) luaL_checknumber (L, -1));
  return 0;
}

static int
lpart_set_drag_page (lua_State * L)
{
  /* 1: part
   * 2: table {dx,dy}
   */
  luaobj_Object *obj = (luaobj_Object *) lua_touserdata (L, 1);
  Evas_Object *edj = evas_object_data_get (obj->data, "edje");
  const char *key = evas_object_data_get (obj->data, "key");
  lua_rawgeti (L, 2, 1);
  lua_rawgeti (L, 2, 2);
  edje_object_part_drag_page_set (edj, key,
				  (int) luaL_checknumber (L, -2),
				  (int) luaL_checknumber (L, -1));
  return 0;
}


/*
 * edje part function definitions
 */

const struct luaL_Reg lPart_fn[] = {
  {"unswallow", lpart_fn_unswallow},

  /* FIXME
     {"drag_step",lpart_fn_drag_step},
     {"drag_page",lpart_fn_drag_page},
   */

  {NULL, NULL}			// sentinel
};

/*
 * edje part get definitions
 */

const struct luaL_Reg lPart_get[] = {
  {"geometry", lpart_get_geometry},
  {"text", lpart_get_text},

  {"swallow", lpart_get_swallow},
  {"state", lpart_get_state},

  {"drag_dir", lpart_get_drag_dir},
  {"drag_value", lpart_get_drag_value},
  {"drag_size", lpart_get_drag_size},
  {"drag_step", lpart_get_drag_step},
  {"drag_page", lpart_get_drag_page},
  {NULL, NULL}			// sentinel
};

/*
 * edje part set definitions
 */

const struct luaL_Reg lPart_set[] = {
  {"swallow", lpart_set_swallow},
  {"text", lpart_set_text},

  {"drag_value", lpart_set_drag_value},
  {"drag_size", lpart_set_drag_size},
  {"drag_step", lpart_set_drag_step},
  {"drag_page", lpart_set_drag_page},
  {NULL, NULL}			// sentinel
};

/*
 * edje part definitions
 */

const luaL_Reg lPart_nil [] = {
	{NULL, NULL} // sentinel
};

const luaobj_Reg mPart = {
  lPart_nil,
  lPart_get,
  lPart_set,
  lPart_fn
};

const luaobj_Reg *cPart[] = {
  &mClass,
  &mObject,
  &mPart,
  NULL
};

