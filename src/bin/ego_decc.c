#include "ego.h"
#include <Eet.h>

int
main (int argc, char **argv)
{
	/*
	 * read in command line arguments
	 */
	const char *fl_path = argv[1];

	/*
	 * initialilze efl libraries
	 */
	eet_init ();

	/*
	 * load and save lua plain and bytecode to file
	 */
	Eet_File *fl_file = eet_open (fl_path, EET_FILE_MODE_READ);
	int bufsize;
	void *buf = eet_read (fl_file, "ego/plain", &bufsize);
	FILE *out = fopen ("plain.lua", "wb");
	fwrite (buf, 1, bufsize, out);
	fclose (out);
	free (buf);
	eet_close (fl_file);

	/*
	 * deinitialize libraries
	 */
	eet_shutdown ();

	return 0;
}

