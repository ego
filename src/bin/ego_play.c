#include "luaobj.h"
#include "evas.h"
#include "ego.h"

#include <Eet.h>
#include <Ecore.h>
#include <Ecore_Config.h>
#include <Ecore_Evas.h>
#include <Evas.h>
#include <Elementary.h>

int
main (int argc, char **argv)
{
	/*
	 * read in command line arguments
	 */
	const char *fl_path = argv[1];
	const int fl_width = atoi (argv[2]);
	const int fl_height = atoi (argv[3]);

	/*
	 * initialilze efl libraries
	 */
	eet_init ();
	ecore_init ();
	ecore_config_init ("ego");
	ecore_evas_init ();
	evas_init ();
	edje_init ();
	elm_init (argc, argv);

	/*
	 * read config
	 */
	char path[256];
	sprintf (path, "%s/%s", CONFIGDIR, "config.edb");
	ecore_config_file_load (path);

	int fps = ecore_config_int_get ("fps");
	ecore_animator_frametime_set (1.0 / (double) fps);
	edje_frametime_set (1.0 / (double) fps);

	sprintf (path, "%s/%s", THEMEDIR, ecore_config_theme_get ("theme"));

	Ecore_Evas *ee = NULL;
	printf ("%s\n", ecore_config_string_get ("engine"));
	int method = evas_render_method_lookup (ecore_config_string_get ("engine"));
	if (method == evas_render_method_lookup ("software_x11"))
		ee = ecore_evas_new ("software_x11", 0, 0, fl_width, fl_height, NULL);
	else if (method == evas_render_method_lookup ("gl_x11"))
		ee = ecore_evas_new ("opengl_x11", 0, 0, fl_width, fl_height, NULL);

	/*
	 * initialize canvas
	 */
	ecore_evas_show (ee);
	Evas *evas = ecore_evas_get (ee);

	/*
	 * initialize theme
	 */
	Evas_Object *theme_obj = edje_object_add (evas);
	edje_object_file_set (theme_obj, path, "ego/theme");	
	edje_object_part_text_set (theme_obj, "message", "loading");
	edje_object_signal_emit (theme_obj, "idle,start", "");
	evas_object_resize (theme_obj, fl_width, fl_height);
	evas_object_focus_set (theme_obj, 1);
	evas_object_show (theme_obj);

	/*
	 * initialize lua state
	 */
	lua_State *L = luaL_newstate ();
	ego_sandbox (L, 4);

	/*
	 * initialize ego library
	 */
	luaopen_ego (L);
	lua_pushstring (L, fl_path);
	lua_setfield (L, LUA_REGISTRYINDEX, "ego_file");

	/*
	 * load and run lua code in ego file
	 */
	Eet_File *fl_file = eet_open (fl_path, EET_FILE_MODE_READ);
	void *globbuf;
	int globbufsize;

	globbuf = eet_read (fl_file, "ego/bytecode", &globbufsize);
	printf ("globbufsize: %d\n", globbufsize);

	/*
	int num, i;
	char **keys = eet_list (fl_file, "*", &num);
	for (i = 0; i < num; i++)
		printf ("%d: %s\n", i, keys[i]);
	free (keys);
	*/

	if (luaL_loadbuffer (L, globbuf, globbufsize, NULL))
		fprintf (stderr, "ego load chunk error: %s\n", lua_tostring (L, -1));
	Evas_Object *ego = ego_object_add (evas, L);
	evas_object_move (ego, 0, 0);
	evas_object_resize (ego, fl_width, fl_height);
	evas_object_show (ego);

	free (globbuf);
	eet_close (fl_file);

	edje_object_part_text_set (theme_obj, "message", "loaded");
	edje_object_signal_emit (theme_obj, "idle,stop", "");
	edje_object_part_swallow (theme_obj, "content", ego);

	/*
	 * enter main loop
	 */
	ecore_main_loop_begin ();

	/*
	 * free objects
	 */
	evas_object_del (ego);
	evas_object_del (theme_obj);
	evas_free (evas);
	ecore_evas_free (ee);

	/*
	 * deinitialize libraries
	 */
	elm_shutdown ();
	edje_shutdown ();
	evas_shutdown ();
	ecore_evas_shutdown ();
	ecore_config_shutdown ();
	ecore_shutdown ();
	eet_shutdown ();

	return 0;
}

