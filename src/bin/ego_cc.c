#include "ego.h"
#include <Eet.h>
#include <Ecore_Evas.h>

void *globbuf;
int globbufsize;

#define BUF_SIZE 1024
char buf [BUF_SIZE];

const char *
_ego_reader (lua_State *L, void *data, size_t *size)
{
	if (feof ((FILE *) data)) return NULL;
	*size = fread (buf, 1, BUF_SIZE, (FILE *) data);

	void *old = globbuf;
	globbuf = malloc (globbufsize + *size);
	memcpy (globbuf, old, globbufsize);
	memcpy (&(globbuf[globbufsize]), buf, *size);
	if (old)
		free (old);
	globbufsize += *size;

  return (*size > 0) ? buf : NULL;
}

int
_ego_writer (lua_State *L, const void* buf, size_t bufsize, void* udata)
{
	void *old = globbuf;
	globbuf = malloc (globbufsize + bufsize);
	memcpy (globbuf, old, globbufsize);
	memcpy (&(globbuf[globbufsize]), buf, bufsize);
	if (old)
		free (old);
	globbufsize += bufsize;
	return 0;
}

int
main (int argc, char **argv)
{
	/*
	 * read in command line arguments
	 */
	const char *l_path = argv[1];
	const char *fl_path = argv[2];

	/*
	 * initialilze efl libraries
	 */
	eet_init ();
	ecore_init ();
	ecore_evas_init ();
	evas_init ();
	edje_init ();
	elm_init (argc, argv);

	/*
	 * initialize canvas
	 */
	Ecore_Evas *ee = ecore_evas_new ("buffer", 0, 0, 1, 1, NULL);
	Evas *evas = ecore_evas_get (ee);

	/*
	 * initialize lua state
	 */
	lua_State *L = luaL_newstate ();
	ego_sandbox (L, 4);

	/*
	 * initialize ego object
	 */
	luaopen_ego (L);
	lua_pushstring (L, fl_path);
	lua_setfield (L, LUA_REGISTRYINDEX, "ego_file");

	/*
	 * load and save lua plain and bytecode in ego file
	 */
	Eet_File *fl_file = eet_open (fl_path, EET_FILE_MODE_READ_WRITE);

	FILE *l_file = fopen (l_path, "rb");
	globbuf = NULL;
	globbufsize = 0;
	lua_load (L, _ego_reader, (void *) l_file, "ego/plain");

	eet_write (fl_file, "ego/plain", globbuf, globbufsize, 1);

	free (globbuf);
	close (l_file);

	lua_pushvalue (L, -1);
	Evas_Object *ego = ego_object_add (evas, L);
	// TODO test object

	globbuf = NULL;
	globbufsize = 0;
	lua_dump (L, _ego_writer, NULL);
	printf ("globbufsize: %d\n", globbufsize);

	eet_write (fl_file, "ego/bytecode", globbuf, globbufsize, 1);

	free (globbuf);
	eet_close (fl_file);

	/*
	 * free objects
	 */
	evas_object_del (ego);
	evas_free (evas);
	ecore_evas_free (ee);

	/*
	 * deinitialize libraries
	 */
	elm_shutdown ();
	edje_shutdown ();
	evas_shutdown ();
	ecore_evas_shutdown ();
	ecore_shutdown ();
	eet_shutdown ();

	return 0;
}

