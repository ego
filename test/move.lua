move = ego:smart_class {
	add = function (self)
		self.box = ego:rectangle ()
	end,

	del = function (self)
		self.box:hide ()
	end,

	move = function (self, x, y)
		self.box:move (x, y)
	end,

	resize = function (self, w, h)
		self.box:resize (w, h)
	end,

	show = function (self)
		self.box:show ()
	end,

	hide = function (self)
		self.box:del ()
	end,

	color_set = function (self, color)
		self.box.color = color
	end,

	clip_set = function (self, clip)
		self.box.clip = clip
	end,

	clip_unset = function (self)
		self.box:clip_unset ()
	end
}

