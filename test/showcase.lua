images = {
	'hrg0001.jpg', 
	'hrg0002.jpg',
	'hrg0003.jpg',
	'hrg0004.jpg',
	'hrg0005.jpg',
	'hrg0006.jpg',
	'hrg0007.jpg',
	'hrg0008.jpg',
	'hrg0009.jpg',
	'hrg0010.jpg',
	'hrg0011.jpg',
	'hrg0012.jpg',
	'hrg0013.jpg',

	'thumb_hrg0001.jpg', 
	'thumb_hrg0002.jpg',
	'thumb_hrg0003.jpg',
	'thumb_hrg0004.jpg',
	'thumb_hrg0005.jpg',
	'thumb_hrg0006.jpg',
	'thumb_hrg0007.jpg',
	'thumb_hrg0008.jpg',
	'thumb_hrg0009.jpg',
	'thumb_hrg0010.jpg',
	'thumb_hrg0011.jpg',
	'thumb_hrg0012.jpg',
	'thumb_hrg0013.jpg'
}

-- box
box = ego:table {
	size_hint_weight = {1, 1},
	homogeneous = true,
	visible = true
}

n = #images / 2
box.edjs = {}
box.thumbs = {}
box.imgs = {}
for i = 0, n-1  do
	box.edjs[i] = ego:edje {
		id = i,
		group = 'icon',
		size_hint_min = {32, 32},
		size_hint_max = {64, 64},
		size_hint_weight = {1, 1},
		size_hint_align = {0, 0},
		size_hint_aspect = {4, 1, 1},
		visible = true,
		callback_mouse_down = function (self, button, x, y, X, Y)
			if lay.new ~= box.imgs[self.id] then
				lay:signal_emit ('fade,out', '')
				lay.old = lay.new
				lay.new = box.imgs[self.id]
			end
		end,
	}
	box.edjs[i]:resize (64, 64)

	box.imgs[i] = ego:image {
		id = i,
		file = images[i],
		name = images[i],
		size_hint_weight = {1, 1},
		callback_resize = function (self) self:fit () end,
		visible = false
	}
	local size = box.imgs[i].size
	box.imgs[i].size_hint_aspect = {4, size[1], size[2]}

	box.thumbs[i] = ego:image {
		id = i + n,
		file = images[i],
		name = images[i],
		size_hint_weight = {1, 1},
		size_hint_aspect = {4, size[1], size[2]},
		callback_resize = function (self) self:fit () end,
		visible = true
	}
	size = box.thumbs[i].size
	box.edjs[i].content.swallow = box.thumbs[i]
	box:pack (box.edjs[i], i-1, 0, 1, 1)
end

-- scroller
scr = ego:scroller {
	size_hint_weight = {1, 1},
	content = box,
	content_min_limit = {false, true}
}

-- layout
lay = ego:edje {
	focus = 1,
	callback_key_down = function (self, key)
		if key == 'Escape' then
			ego:quit ()
		elseif key == 't' then
			if ego.visible then
				ego:hide ()
			else
				ego:show ()
			end
		end
	end,
	layer = 1,
	group = 'layout',
	visible = true,
	size = {1, 1},
	signal_callback = function (self, signal, source)
		if signal == 'faded,out' then
			lay.bg_content:unswallow ()
			lay.old.visible = false
			lay.new.visible = true
			lay.bg_content.swallow = lay.new
			lay:signal_emit ('fade,in', '')
		end
	end
}
lay.new = box.imgs[1]
lay.new.visible = true
lay.bg_content.swallow = lay.new

lay.bar_content.swallow = scr
lay:signal_emit ('fade,in', '')

