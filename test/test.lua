-- compile time definitions

WIDTH = 400
HEIGHT = 400

images = {
	['frywintux.jpeg'] = 0
}

--require 'move'

-- edje
edj = {}
n = 16
for i = 1, n do
	edj[i] = {}
	for j = 1, n do
		edj[i][j] = ego:edje {
			name = 'icon_' .. i .. '_' .. j,
			group = 'icon',
			pos = {(i-1)/n, (j-1)/n},
			size = {1/n, 1/n},
			visible = true
		}

		edj[i][j].rect = ego:rectangle {
			name = 'rect_' .. i .. '_' .. j,
			color = {i*255/n, j*255/n, 0, 255}
		}

		edj[i][j].container.swallow = edj[i][j].rect
	end
end

-- image
img = ego:image {
	repeat_events = true,
	file = images['frywintux.jpeg'],
	color = {255, 255, 255, 128},
	moveto = function (self, x, y, v)
		self.animator = coroutine.wrap (function (anim)
			geo = anim.ptr.geometry
			if x > geo[1] then
				e = v
			else
				e = -v
			end
			for X = geo[1], x, e do
				anim.ptr:move (X, geo[2])
				coroutine.yield (true)
			end
			anim.ptr.anim = nil
			return false
		end)
		if (self.anim) then
			self.anim:del ()
		end
		self.anim = ego:animator (self.animator)
		self.anim.ptr = self
	end,
	callback_resize = function (self) self:fit () end,
	pos = {0.3, 0.4},
	size = {0.3, 0.4},
	visible = true
}

-- rectangle
overlay = ego:rectangle {
	focus = true,
	color = {0, 0, 0, 0},
	repeat_events = true,

	callback_key_down = function (self, key)
		if key == 'Escape'
			then ego:quit ()
		elseif key == 't'
			then ego:hide ()
		end
	end,

	callback_mouse_down = function (self, button, x, y, X, Y)
		img:moveto (x, y, 5)
	end,
	pos = {0, 0},
	size = {1, 1},
	visible = true
}

-- smart
--[[
self.mv = ego:smart (move)
mv:set {
	color = {255, 0, 0, 128},
	visible = true
}
mv:move (100, 100)
mv:resize (100, 100)
--]]

-- gradient
gra = ego:gradient {
	angle = 45,
	fill = {0, 0, HEIGHT, WIDTH},
	repeat_events = true,

	colors = {
		{0,0,0,0,0},
		{0,0,0,255,100},
	},

	animator = coroutine.wrap( function (self)
		while true do
			for i = 0, 360, 2 do
				gra.angle = i
				coroutine.yield (true)
			end
		end
	end),
	visible = true,
	size = {1, 1}
}
gra:clear()
for _,v in ipairs (gra.colors) do
	gra:color_stop_add (v)
end
--gra.anim = ego:animator (gra.animator)

-- line
line = ego:line {
	repeat_events = true,
	xy = {
		math.random (WIDTH),
		math.random (HEIGHT),
		math.random (WIDTH),
		math.random (HEIGHT)
	},
	visible = true
}

-- polygon
poly = ego:polygon {
	pass_events = true,
	color = {255,255,0,32},

	points = {
		{50,100},
		{10,200},
		{30,50}
	},
	visible = true
}
for _,v in ipairs (poly.points) do
	poly:point_add (v[1], v[2])
end

-- text
tex = ego:text {
	repeat_events = true,
	style = 4,
	font_source = 'test.ego',
	font = {'default', 20},
	text = 'hello world',
	color = {255, 255, 255, 255},
	outline_color = {0, 0, 0, 255},
	glow_color = {0, 0, 255, 255},
	glow2_color = {0, 255, 0, 255},
	shadow_color = {255, 0, 0, 128} ,
	visible = true
}

-- textblock style
sty = ego:textblock_style ()
sty.style = [[
	DEFAULT='font=notepad.ttf font_size=20 align=center color=#000 style=outline_shadow outline_color=#000 shadow_color=#fff wrap=word'
	r='+color=#f00'
	g='+color=#0f0'
	b='+color=#00f'
	y='+color=#ff0'
]]

-- textblock
tb = ego:textblock {
	repeat_events = true,
	style = sty,
	text_markup = '<r>hello</r> world, <g>whats</g> up? <b>are</b> you <y>alright</y>?',

	pos = {0, 0.25},
	size = {1, 0.25},
	visible = true
}

-- button
b = ego:button {
	label = 'hello',
	clicked = coroutine.wrap (function (self)
		for i = 1, 10 do
			print (i)
			coroutine.yield ()
		end
		self:hide ()
	end),
	visible = true
}

-- scroller
scr = ego:scroller {
	content = b,
	content_min_limit = {true, false},
	visible = true,
	pos = {0.2, 0.2},
	size = {0.5, 0.1}
}

-- label
lab = ego:label {
	label = 'I am a label',
	visible= true,
	pos = {0.6, 0.6}
}

-- toggle
tog = ego:toggle {
	states_labels = {'off', 'on'},
	state = true,
	changed = function (self)
		print (self.state)
	end,
	visible = true
}

-- frame
frm = ego:frame {
	label = 'test frame',
	content = tog,
	visible = true,
	pos = {0.7, 0.7}
}

-- table
tbl = ego:table {
	homogenous = true,
	visible = true,
	pos = {0.1, 0.1},
	size = {0.3, 0.3}
}
for i = 0, 3 do
	for j = 0, 3 do
		r = ego:rectangle {
			color = {64*i, 64*j, 0, 255},
			visible = true,
			size = {0.1, 0.1}
		}
		tbl:pack (r, i, j, 1, 1)
	end
end

-- entry
ent = ego:entry {
	single_line = true,
	password = false,
	entry = 'edit me',
	line_wrap = false,
	editable = true,
	visible = true,

	['changed'] = function (self) print 'changed' end,
	['selection,start'] = function (self) print 'selection,start' end,
	['selection,changed'] = function (self) print 'selection,changed' end,
	['selection,cleard'] = function (self) print 'selection,cleared' end,
	['selection,paste'] = function (self) print 'selection,paste' end,
	['selection,copy'] = function (self) print 'selection,copy' end,
	['selection,cut'] = function (self) print 'selection,cut' end,
	['cursor,changed'] = function (self) print 'cursor,changed' end,
	['anchor,clicked'] = function (self) print 'anchor,clicked' end,
	['activated'] = function (self) print 'activated' end,
}

-- transform
trans = ego:transform {
	matrix = {
		{1, 0, 0},
		{0, 1, 0},
		{0, 0, 1}
	}
}
trans:rotate (33)

-- gradient2
gra2 = ego:gradient2_linear {
	fill_spread = 0,
	fill_transform = trans,
	fill = {0, 0, 50, 50},

	visible = true,
	pos = {0, 0.8},
	size = {0.2, 0.2},
	transient = {
		{255, 0, 0, 255, 0.0},
		{0, 255, 0, 255, 0.2},
		{0, 0, 255, 255, 0.7},
		{0, 255, 255, 255, 1.0}
	}
}
for _, v in ipairs (gra2.transient) do
	gra2:color_np_stop_insert (v)
end

-- bubble
bub = ego:bubble {
	label = 'I am a bubble',
	info = 'I really am a bubble',
	icon = ego:rectangle {
		color = {255, 0, 0, 255},
		visible = true
	},
	content = ent,
	visible = true,
	pos = {0.3, 0.8},
	size = {0.7, 0.2}
}

